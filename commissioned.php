<?php
include 'dbconfig.php';
$email = $_SESSION['email'];

$get_login =$DB_con->prepare(" select * from login WHERE email = '$email' ");
$get_login->execute();
$login = $get_login->fetch();
?>
<!DOCTYPE html>
<html>

<head>
	<title>Venika | Commissioned </title>
	<!--/tags -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="Conceit Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
	<script type="application/x-javascript">
		addEventListener("load", function () {
			setTimeout(hideURLbar, 0);
		}, false);

		function hideURLbar() {
			window.scrollTo(0, 1);
		}
	</script>
	<!--//tags -->
	<link rel="shortcut icon" href="images/home/venika-icon.png"/>
	<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
	<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
	<link href="css/custom.css" rel="stylesheet" type="text/css" media="all" />
	<link href="css/team.css" rel="stylesheet" type="text/css" media="all" />
	<link href="css/font-awesome.css" rel="stylesheet">
	<!-- //for bootstrap working -->
	<link href="//fonts.googleapis.com/css?family=Raleway:100,100i,200,300,300i,400,400i,500,500i,600,600i,700,800" rel="stylesheet">
	<link href="//fonts.googleapis.com/css?family=Source+Sans+Pro:300,300i,400,400i,600,600i,700" rel="stylesheet">
	<!-- tab-&-img-modal-popup -->
	<link rel="stylesheet" href="w3css/4/w3.css">
	<!-- //End -->
</head>

<body style="text-align: justify;">
	<!-- header-top -->
	<div class="top_header" id="home">
		<!-- Fixed navbar -->
		<nav class="navbar navbar-default navbar-fixed-top" style="border-bottom: 2px solid #118eaf;">
			<div class="nav_top_fx_w3ls_agileinfo">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false"
					    aria-controls="navbar">
				        <span class="sr-only">Toggle navigation</span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				    </button>
					<div class="logo-w3layouts-agileits">
						<h1>
							<a class="navbar-brand" href="index.php">
								<!-- <i class="fa fa-clone" aria-hidden="true"></i> Conceit <span class="desc">For your Business</span> -->
								<img src="images/home/venika-logo-head.png" alt="" class="img-responsive">
							</a>
						</h1>
					</div>
				</div>
				<div id="navbar" class="navbar-collapse collapse">
					<div class="nav_right_top">
						<ul class="nav navbar-nav">
							<li><a href="index.php">Home</a></li>
							<li><a href="about.php">About Us</a></li>
							<li><a href="management.php">Management</a></li>
							<li class="dropdown active">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Projects <span class="caret"></span></a>
								<ul class="dropdown-menu">
									<li><a href="under-implementation.php">UNDER IMPLEMENTATION</a></li>
									<li><a href="under-construction.php">UNDER CONSTRUCTION</a></li>
									<li class="active"><a href="commissioned.php">COMMISSIONED</a></li>
								</ul>
							</li>
							<li><a href="social-responsibility.php">Social Responsibility</a></li>
							<li><a href="contact.php">Contact</a></li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user"></i>    <?php echo $login['name']; ?> <span class="caret"></span></a>
								<ul class="dropdown-menu">
									<li><a href="logout.php">Logout</a></li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
				<!--/.nav-collapse -->
			</div>
		</nav>
	</div>
	<!-- //End -->

	<!--/banner_info-->
	<div class="banner_inner_con"> </div>
	<div class="services-breadcrumb">
		<div class="inner_breadcrumb">
			<ul class="short">
				<li><a href="index.php">Home</a><span>|</span></li>
				<li>Commissioned</li>
			</ul>
		</div>
	</div>
	<!--//banner_info-->
	
	<!--/bottom-->
	<div class="banner_bottom">
		<div class="container">
			<div class="title-underline">
				<h3 class="tittle-w3ls">Commissioned</h3>
			</div>
			<?php
              $stmt_core_value = $DB_con->prepare('SELECT * FROM commissioned where data_delete = 0 ORDER BY com_id desc');
              $stmt_core_value->execute();

              if($stmt_core_value->rowCount() > 0)
              {
              while($row_core_value=$stmt_core_value->fetch(PDO::FETCH_ASSOC))
              {
              extract($row_core_value);
              ?>
               <div class="inner_sec_info_wthree_agile">
				<div class="help_full">
					<div class="col-md-6 banner_bottom_left" style="padding: 10% 10% 0 10%;">
						<h4><?php echo $row_core_value['com_name']; ?></h4>
						<p><b>Capacity :</b> <?php echo $row_core_value['com_capacity']; ?></p>
						<p><b>Location :</b> <?php echo $row_core_value['com_location']; ?></p>
						<p><b>State :</b> <?php echo $row_core_value['com_state']; ?></p>
						<div class="commissioned-underline"> </div>
					</div>
					<div class="col-md-6 banner_bottom_grid help">
						<img src="admin/db/com_image/<?php echo $com_img; ?>" alt=" " class="img-responsive">
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
              <?php
              }
              }
              else
              {
              ?>
              <div class="col-xs-12">
              <div class="alert alert-warning">
              <span class="glyphicon glyphicon-info-sign"></span> &nbsp; No Data Found ...
              </div>
              </div>
              <?php
              }

              ?>
			<!-- <div class="inner_sec_info_wthree_agile">
				<div class="help_full">
					<div class="col-md-6 banner_bottom_left" style="padding: 10% 10% 0 10%;">
						<h4>H Malligere SHP</h4>
						<p><b>Capacity :</b> 0.75 MW</p>
						<p><b>Location :</b> Maddur Branch Canal</p>
						<p><b>State :</b> Karnataka</p>
						<div class="commissioned-underline"> </div>
					</div>
					<div class="col-md-6 banner_bottom_grid help">
						<img src="images/home/commissioned.jpg" alt=" " class="img-responsive">
					</div>
					<div class="clearfix"></div>
				</div>
			</div> -->
		</div>
	</div>
	<!--//bottom-->

	<!--/what-->
	<div class="works" style="background: none; padding-top: 0;">
		<div class="container">
			<h3 class="tittle-w3ls">Projects</h3>
			<div class="inner_sec_info_wthree_agile">
				<div class="ser-first" style="border: 2px solid rgb(118, 218, 255); padding: 5px;">
				    <!-- Owl image Popup Start -->
				   <?php

					        $stmt_slider_pop = $DB_con->prepare('SELECT * FROM res_slider where add_slider_page = 2 and data_delete = 0 ORDER BY res_slider_id DESC limit 20');
					        $stmt_slider_pop->execute();
					        
					        if($stmt_slider_pop->rowCount() > 0)
					        {
					        while($row_slider_pop=$stmt_slider_pop->fetch(PDO::FETCH_ASSOC))
					        {
					          extract($row_slider_pop);
					          ?>
				    <div id="popup_<?php echo $row_slider_pop['res_slider_id'];?>" class="w3-modal w3-modal-img-pop" onclick="this.style.display='none'">
    				    <span class="w3-button-img-pop w3-hover-red w3-xlarge-img-pop w3-display-topright-img-pop">&times;</span>
    				    <div class="w3-modal-content-img-pop w3-animate-zoom">
    				      	<img src="admin/db/slider_image/<?php echo $row_slider_pop['res_slider_image'];?>" class="img-responsive">
    				    </div>
    				</div>
    				<?php
				        }
				        }
				        else
				        {

				        ?>
			              <div class="col-xs-12">
			               <div class="alert alert-warning" style="font-size: 12px !important; background: #fff; border: none; padding: 1em 0 1em 0;">
			                 <span class="glyphicon glyphicon-info-sign"></span> &nbsp; No Data Found ...
			                </div>
			              </div>
			              <?php
				        }
				        
				      ?>
					<div id="owl-demo">
						<?php

					        $stmt_slider = $DB_con->prepare('SELECT * FROM res_slider where add_slider_page = 2 and data_delete = 0 ORDER BY res_slider_id DESC limit 20');
					        $stmt_slider->execute();
					        
					        if($stmt_slider->rowCount() > 0)
					        {
					        while($row_slider=$stmt_slider->fetch(PDO::FETCH_ASSOC))
					        {
					          extract($row_slider);
					          ?>
						<div class="item">
						    <img src="admin/db/slider_image/<?php echo $row_slider['res_slider_image'];?>" class="img-responsive w3-hover-opacity" onclick="document.getElementById('popup_<?php echo $row_slider['res_slider_id'];?>').style.display='block'" style="cursor:zoom-in; width: 200px;height: 150px;" alt="Owl Image-1">
						</div>
						<?php
					        }
					        }
					        else
					        {

					        ?>
					              <div class="col-xs-12">
					               <div class="alert alert-warning" style="font-size: 12px !important; background: #fff; border: none; padding: 1em 0 1em 0;">
					                 <span class="glyphicon glyphicon-info-sign"></span> &nbsp; No Data Found ...
					                </div>
					              </div>
					              <?php
					        }
					        
					      ?>
					</div>
					<!-- //End -->
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>
	<!--//what-->
	
	<!-- Footer-start-here -->
	<?php include 'footer.php'; ?>
	<!-- //Footer-end-here -->
	
	<!-- js -->
	<script type="text/javascript" src="js/jquery-2.2.3.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.js"></script>
	<script>
		$('ul.dropdown-menu li').hover(function () {
			$(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
		}, function () {
			$(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
		});
	</script>
	<!-- start-smoth-scrolling -->
	<script type="text/javascript" src="js/move-top.js"></script>
	<script type="text/javascript" src="js/easing.js"></script>
	<script type="text/javascript">
		jQuery(document).ready(function ($) {
			$(".scroll").click(function (event) {
				event.preventDefault();
				$('html,body').animate({
					scrollTop: $(this.hash).offset().top
				}, 900);
			});
		});
	</script>
	<!-- start-smoth-scrolling -->


	<!-- stats -->
	<script src="js/jquery.waypoints.min.js"></script>
	<script src="js/jquery.countup.js"></script>
	<script>
		$('.counter').countUp();
	</script>
	<!-- //stats -->
	<script type="text/javascript">
		$(document).ready(function () {
			/*
									var defaults = {
							  			containerID: 'toTop', // fading element id
										containerHoverID: 'toTopHover', // fading element hover id
										scrollSpeed: 1200,
										easingType: 'linear' 
							 		};
									*/

			$().UItoTop({
				easingType: 'easeOutQuart'
			});

		});
	</script>
	<a href="#home" class="scroll" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>

	<!-- owl-carousel -->
	<script type="text/javascript" src="js/jquery-2.2.4.min.js"></script>
	<script type="text/javascript" src="js/owl-carousel.js"></script>
	<!-- //End -->

</body>
</html>