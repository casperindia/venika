<!-- Footer-start-here -->
	<!-- newsletter -->
	<div class="newsletter_w3ls_agileits footer-bg-col">
		<div class="container">
			<!--<div class="col-sm-2 newsleft">
			    <h3><img src="images/home/venika-logo-footer.png" alt="" class="img-responsive" style="width: 50px;"></h3>
				<h3>VENIKA<span>&reg;</span></h3>
			</div>-->
			<div class="col-sm-8 newsmiddle" style="float: left;">
				<p class="copy-right" style="float: left;">&copy; <?php echo date("Y"); ?> VENIKA. All rights reserved | Design by <a href="https://casperindia.com/" target="_blank;">CasperIndia</a></p>
			</div>
			<div class="col-sm-2 newsright" style="float: right;">
				<div class="address-left mail-icon">
					<i class="fa fa-envelope" aria-hidden="true"></i>
				</div>
				<p class="contact-mail">
					<a href="mailto:info@venika.in"> info@venika.in</a>
				</p>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<!-- //newsletter-->
	<script>
$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();   
});
</script>
<!-- //Footer-end-here -->