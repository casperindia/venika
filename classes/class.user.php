<?php
class User extends Password{
    private $db;
	function __construct($db){
		parent::__construct();
		$this->_db = $db;
	}
	public function is_logged_in(){
		if(isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true){
			return true;
		}
	}
	private function get_user_hash($email){
		try {
			$stmt = $this->_db->prepare('SELECT * FROM login WHERE email = :email');
			$stmt->execute(array('email' => $email));
			return $stmt->fetch();
		} catch(PDOException $e) {
		    echo '<p class="error" style="color:red;">'.$e->getMessage().'</p>';
		}
	}
	public function login($email){
		$user = $this->get_user_hash($email);
		if(($user['status'] == '1')){
		    $_SESSION['loggedin'] = true;
		    $_SESSION['id'] = $user['id'];
		    $_SESSION['email'] = $user['email'];
		    return true;
		}
	}
	public function logout(){
		session_destroy();
	}
}
?>
