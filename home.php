<?php
/*include 'admin/db/dbconfig.php';*/

require_once 'admin/db/dbconfig.php';
$email = $_SESSION['email'];

$get_login =$DB_con->prepare(" select * from login WHERE email = '$email' ");
$get_login->execute();
$login = $get_login->fetch();

$get_about_about_us =$DB_con->prepare("select * from  home_aboutus WHERE home_about_id = '1'");
$get_about_about_us->execute();
$about_about_us = $get_about_about_us->fetch();

$get_banner =$DB_con->prepare("select * from banner_video WHERE banner_id = '1'");
$get_banner->execute();
$banner_video = $get_banner->fetch();

$get_add_video =$DB_con->prepare("select * from add_video WHERE add_video_id = '1'");
$get_add_video->execute();
$add_video = $get_add_video->fetch();

$get_one_responsibility =$DB_con->prepare(" select * from social_responsibility WHERE res_id = '1'");
$get_one_responsibility->execute();
$get_one_r = $get_one_responsibility->fetch();

$get_two_responsibility =$DB_con->prepare(" select * from social_responsibility WHERE res_id = '2'");
$get_two_responsibility->execute();
$get_two_r = $get_two_responsibility->fetch();

$get_three_responsibility =$DB_con->prepare(" select * from social_responsibility WHERE res_id = '3'");
$get_three_responsibility->execute();
$get_three_r = $get_three_responsibility->fetch();

$get_four_responsibility =$DB_con->prepare(" select * from social_responsibility WHERE res_id = '4'");
$get_four_responsibility->execute();
$get_four_r = $get_four_responsibility->fetch();

if(isset($_SESSION['email'])){ 
?>


<!DOCTYPE html>
<html>

<head>
	<title>Venika | Home </title>
	<!--/tags -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="Conceit Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
	<script type="application/x-javascript">
		addEventListener("load", function () {
			setTimeout(hideURLbar, 0);
		}, false);

		function hideURLbar() {
			window.scrollTo(0, 1);
		}
	</script>
	<!--//tags -->
	<link rel="shortcut icon" href="images/home/venika-icon.png"/>
	<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
	<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
	<link href="css/custom.css" rel="stylesheet" type="text/css" media="all" />
	<link href="css/team.css" rel="stylesheet" type="text/css" media="all" />
	<link href="css/prettyPhoto.css" rel="stylesheet" type="text/css" />
	<link href="css/font-awesome.css" rel="stylesheet">
	<!-- //for bootstrap working -->
	<link href="//fonts.googleapis.com/css?family=Raleway:100,100i,200,300,300i,400,400i,500,500i,600,600i,700,800" rel="stylesheet">
	<link href="//fonts.googleapis.com/css?family=Source+Sans+Pro:300,300i,400,400i,600,600i,700" rel="stylesheet">
	<!-- Slidre -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.1/css/swiper.min.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
	<link href="https://fonts.googleapis.com/css?family=Oswald:500" rel="stylesheet">
	<script>!function(e){"undefined"==typeof module?this.charming=e:module.exports=e}(function(e,n){"use strict";n=n||{};var t=n.tagName||"span",o=null!=n.classPrefix?n.classPrefix:"char",r=1,a=function(e){for(var n=e.parentNode,a=e.nodeValue,c=a.length,l=-1;++l<c;){var d=document.createElement(t);o&&(d.className=o+r,r++),d.appendChild(document.createTextNode(a[l])),n.insertBefore(d,e)}n.removeChild(e)};return function c(e){for(var n=[].slice.call(e.childNodes),t=n.length,o=-1;++o<t;)c(n[o]);e.nodeType===Node.TEXT_NODE&&a(e)}(e),e});
	</script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.1/js/swiper.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.0.2/TweenMax.min.js"></script>
	<!-- //End -->
    
    <!-- video-banner-title -->
	<style type="text/css">
	#video-banner-title {
	  /*background-color: white;*/
	  background: #ffffff6b;
	  color: #000000;
	  /*font-size: 10vw;*/ 
	  font-weight: bold;
	  margin: 0 auto;
	  padding: 10px;
	  width: 50%;
	  text-align: center;
	  position: absolute;
	  top: 50%;
	  left: 50%;
	  transform: translate(-50%, -50%);
	  mix-blend-mode: screen;
	  border: 2px solid #fff;
	}
	/*@media (min-width: 991px) {
        #video-banner-title {
            font-size: 10vw;
            width: 100%;
            top: 50%;
            left: 50%;
        }
    }*/
    .ab_button{
    	margin-top:0px;
    }
	</style>
	<!-- //End -->
</head>
<body>
	<!-- header-top -->
	<div class="top_header" id="home">
		<!-- Fixed navbar -->
		<nav class="navbar navbar-default navbar-fixed-top">
			<div class="nav_top_fx_w3ls_agileinfo">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
				        <span class="sr-only">Toggle navigation</span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				    </button>
					<div class="logo-w3layouts-agileits">
						<h1>
							<a class="navbar-brand" href="index.php">
								<!-- <i class="fa fa-clone" aria-hidden="true"></i> Conceit <span class="desc">For your Business</span> -->
								<img src="images/home/venika-logo-head.png" alt="" class="img-responsive">
							</a>
						</h1>
					</div>
				</div>
				<div id="navbar" class="navbar-collapse collapse">
					<div class="nav_right_top">
						<ul class="nav navbar-nav">
							<li class="active"><a class="nav-link" href="home.php">Home</a></li>
							<li><a class="nav-link" href="about.php">About Us</a></li>
							<li><a class="nav-link" href="management.php">Management</a></li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Projects <span class="caret"></span></a>
								<ul class="dropdown-menu">
									<li><a href="under-implementation.php">UNDER IMPLEMENTATION</a></li>
									<li><a href="under-construction.php">UNDER CONSTRUCTION</a></li>
									<li><a href="commissioned.php">COMMISSIONED</a></li>
								</ul>
							</li>
							<li><a class="nav-link" href="social-responsibility.php">Social Responsibility</a></li>
							<li><a class="nav-link" href="contact.php">Contact</a></li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user"></i>    <?php echo $login['name']; ?> <span class="caret"></span></a>
								<ul class="dropdown-menu">
									<li><a href="logout.php">Logout</a></li>
								</ul>
							</li>
							<!-- <li><span style="font-size: 18px;" class="fa fa-user-circle" aria-hidden="true"></span></li>
							<li><a href="logout.php"><span style="font-size: 18px;" class="fa fa-power-off" aria-hidden="true"></span></a></li> -->
							
						</ul>
					</div>
				</div>
				<!--/.nav-collapse -->
			</div>
		</nav>
	</div>
	<!-- //End -->

	<!-- Slider -->
	<section>
	    <!-- banner-video-start -->
		<div class="swiper-container slideshow">
			<div class="swiper-wrapper">
				<div class="swiper-slide slide">
					<!-- <iframe width="1600" height="900" src="https://www.youtube.com/embed/tgbNymZ7vqY"></iframe> -->

					<!-- <iframe width="1600" height="900" src="<?php echo $banner_video['banner_video']; ?>?rel=0&amp;controls=0&amp;loop=1&amp;showinfo=0;autoplay=1&mute=1" frameborder="0" allow="accelerometer; autoplay; loop; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> -->

					<!-- <iframe width="1600" height="900" src="<?php echo $banner_video['banner_video']; ?>?rel=0&amp;controls=0&amp;loop=1&amp; showinfo=0;autoplay=1&mute=1" frameborder="0" allow="accelerometer; autoplay; loop; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> -->

					<!-- <iframe width="1600" height="900" src="<?php echo $banner_video['banner_video']; ?>?rel=0&amp;controls=1&amp;loop=1&amp; showinfo=0;autoplay=1&mute=1" frameborder="0" allow="accelerometer; encrypted-media;" allowfullscreen></iframe> -->
                   
                   <iframe width="1600" height="900" src="<?php echo $banner_video['banner_video']; ?>?autoplay=1&loop=1&playlist=<?php echo $banner_video['banner_video_id']; ?>" allow="accelerometer; autoplay; loop; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
					<!-- <iframe width="100%" height="100%" src="http://www.youtube.com/embed/GRonxog5mbw?rel=0&amp;controls=1&amp;loop=1&amp; showinfo=0;autoplay=1&mute=1" allow="autoplay; encrypted-media" frameborder="0" allowfullscreen></iframe>​ -->
                    
                    <span class="slide-title" id="video-banner-title">Hydro Energy</span>
				</div>
			</div>
		</div>
		<!-- //banner-video-end -->
		
		
	</section>
	<!-- //End -->

	<div class="marquee">
		<marquee id="marquee" behavior="scroll" direction="left" onmouseover="this.stop();" onmouseout="this.start();">
       		<a href="about.php">
       			Venika has been successfully operating in the Indian Renewable Energy sector since 2005. The company has successfully developed and commissioned a canal based Mini Hydel Project in Karnataka (0.75 MW).
       		</a>
       	</marquee>
	</div>

	<!--/ab-->
	<div class="banner_bottom" style="padding: 4em 0;">
		<div class="container">
			<div class="title-underline">
				<h3 class="tittle-w3ls"><?php echo $about_about_us['home_about_title_one']; ?></h3>
			</div>
			<div class="inner_sec_info_wthree_agile">
				<div class="help_full">
					<div class="col-md-6 banner_bottom_grid help">
					    <a href="about.php">
						 <img src="admin/db/about_image/<?php echo $about_about_us['home_about_image']; ?>" alt=" " class="img-responsive" style="width: 100%; height: 330px;">
					    </a>
					</div>
					<div class="col-md-6 banner_bottom_left" style="border-left: 2px solid rgb(118, 218, 255);height: 330px;">
						<h4><?php echo $about_about_us['home_about_title_two']; ?></h4>
						 <p><?php echo $about_about_us['home_about_desc']; ?></p> 
						<!-- <p>
						 <?php 
                           $about_about_us = $about_about_us['home_about_desc'];
                  		   if (strlen($about_about_us) > 200) $about_about_us = substr($about_about_us, 0, 400).'...';
                           echo $about_about_us;
                           ?>
                         </p> -->
						<div class="ab_button">
							<a class="btn btn-primary btn-lg hvr-underline-from-left abt" href="about.php" role="button" >Read More </a>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>
	<!--//ab-->

	<!-- Management -->
	<div class="banner_bottom proj management-proj">
		<div class="container" style="padding: 55px;">
			<h3 class="tittle-w3ls">Management</h3>
	      <?php
              $stmt_leadership = $DB_con->prepare('SELECT * FROM leadership where data_delete = 0 ORDER BY leader_id desc');
              $stmt_leadership->execute();

              if($stmt_leadership->rowCount() > 0)
              {
              while($leadership=$stmt_leadership->fetch(PDO::FETCH_ASSOC))
              {
              extract($leadership);
              ?>
            <div class="row" onclick="myFunction2()" style="cursor: pointer;">
			    <div class="col-sm-6">
			      <div class="our-team">
			        <div class="picture">
			          <img class="img-fluid" src="admin/db/leader_image/<?php echo $leadership['leader_image']; ?>">
			        </div>
			        <div class="team-content">
			          <h3 class="name"><?php echo $leadership['leader_name']; ?></h3>
			          <h4 class="title"><?php echo $leadership['leader_des']; ?></h4>
			        </div>
			        <ul class="social">
			          <!--<li><a href="#">Click </a></li>-->
			        </ul>
			      </div>
			    </div>
              <?php
              }
              }
              else
              {
              ?>
              	<div class="col-xs-12">
              		<div class="alert alert-warning">
              			<span class="glyphicon glyphicon-info-sign"></span> &nbsp; No Data Found ...
              		</div>
              	</div>
              <?php
              }

              ?>
            </div>
        </div>
	</div>

	<!--/banner_bottom-->
	<div class="banner_bottom">
		<div class="banner_bottom_in">
			<h3 class="tittle-w3ls we"><?php echo $add_video['add_video_title']; ?></h3>
			<p><?php echo $add_video['add_video_desc']; ?></p>
			<!--  <img src="images/home/coming-soon.jpg" class="img-responsive" alt="">  -->
			 <iframe width="100%" height="100%" src="<?php echo $add_video['add_video']; ?>"  >
              </iframe> 

			<!--<video controls="all" muted loop id="myVideo-mid" style="width: 100%;">
			  	<source src="video/mid-pop.mp4" type="video/mp4">
			</video>-->
		</div>
	</div>
	<!--//banner_bottom-->

	<!--/projects-->
	<div class="banner_bottom proj" style="padding: 0;margin-bottom: 7em;">
		<div class="wrap_view">
			<h3 class="tittle-w3ls">Projects</h3>
			<div class="inner_sec">
				<ul class="portfolio-area">
					<?php
		              $get_home_projects = $DB_con->prepare('SELECT * FROM home_projects where data_delete = 0 ORDER BY home_project_id desc');
		              $get_home_projects->execute();

		              if($get_home_projects->rowCount() > 0)
		              {
		              while($home_projects=$get_home_projects->fetch(PDO::FETCH_ASSOC))
		              {
		              extract($home_projects);
		              ?>
		              <li class="portfolio-item2" data-id="id-0" data-type="cat-item-4">
		            <div>
							<span class="image-block img-hover">
								<a class="image-zoom" href="admin/db/project_images/<?php echo $home_projects['home_project_image']; ?>" rel="prettyPhoto[gallery]">
									<img src="admin/db/project_images/<?php echo $home_projects['home_project_image']; ?>" class="img-responsive" alt="Conceit">
									
								</a>
								<a href="under-implementation.php">
								    <div class="port-info">
										<h5><?php echo $home_projects['home_project_section']; ?></h5>
										<p><?php echo $home_projects['home_project_name']; ?> | <?php echo $home_projects['home_project_place']; ?></p>
									</div>
								</a>
							</span>
						</div>
		              <?php
		              }
		              }
		              else
		              {
		              ?>
		              	<div class="col-xs-12">
		              		<div class="alert alert-warning">
		              			<span class="glyphicon glyphicon-info-sign"></span> &nbsp; No Data Found ...
		              		</div>
		              	</div>
		              <?php
		              }

		              ?>
		          </li>
					<div class="clearfix"></div>
				</ul>
				<!--end portfolio-area -->
			</div>
		</div>
	</div>
	<!--//projects-->
	
	<!--/blog-->

	<div class="blog_sec" style="padding: 0; margin-bottom: 7em;">
		<h3 class="tittle-w3ls">Social Responsibility</h3>
		<div class="col-md-6 banner-btm-left">
			<div class="banner-btm-top">
				<div class="banner-btm-inner a1">
					<h6><a href="#"><?php echo $get_one_r['res_name']; ?></a></h6>
					<p class="paragraph"><?php echo $get_one_r['res_desc']; ?></p>
					<div class="clearfix"></div>
				</div>
				<div class="banner-btm-inner a2" style=" background: url(admin/db/social_images/<?php echo $get_one_r['res_image']; ?>) no-repeat 0px 0px/cover;">

				</div>
			</div>
			<div class="banner-btm-bottom">
				<div class="banner-btm-inner a3" style=" background: url(admin/db/social_images/<?php echo $get_two_r['res_image']; ?>) no-repeat 0px 0px/cover;">

				</div>
				<div class="banner-btm-inner a4">
					<h6><a href="#"><?php echo $get_two_r['res_name']; ?></a></h6>
					<p class="paragraph"><?php echo $get_two_r['res_desc']; ?></p>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
		<div class="col-md-6 banner-btm-left">
			<div class="banner-btm-top">
				<div class="banner-btm-inner a1">
					<h6><a href="#"><?php echo $get_three_r['res_name']; ?></a></h6>
					<p class="paragraph"><?php echo $get_three_r['res_desc']; ?></p>
					<div class="clearfix"></div>
				</div>
				<div class="banner-btm-inner a5"  style=" background: url(admin/db/social_images/<?php echo $get_three_r['res_image']; ?>) no-repeat 0px 0px/cover;">
               </div>
			</div>
			<div class="banner-btm-bottom">
				<div class="banner-btm-inner a6"  style=" background: url(admin/db/social_images/<?php echo $get_four_r['res_image']; ?>) no-repeat 0px 0px/cover;">
                </div>
				<div class="banner-btm-inner a4">
					<h6><a href="#"><?php echo $get_four_r['res_name']; ?></a></h6>
					<p class="paragraph"><?php echo $get_four_r['res_desc']; ?></p>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
	

	<!--/what-->
	<div class="works" style="background: none; padding-top: 0;">
		<div class="container">
			<h3 class="tittle-w3ls">Recent Projects</h3>
			<div class="inner_sec_info_wthree_agile">
				<div class="ser-first" style="border: 2px solid rgb(118, 218, 255); padding: 5px;">
					<!-- owl-carousel -->
					<div id="owl-demo">
						<?php
			              $stmt_core_value = $DB_con->prepare('SELECT * FROM under_implementation where data_delete = 0 ORDER BY under_imp_id asc');
			              $stmt_core_value->execute();

			              if($stmt_core_value->rowCount() > 0)
			              {
			              while($row_core_value=$stmt_core_value->fetch(PDO::FETCH_ASSOC))
			              {
			              extract($row_core_value);
			              ?>
			              <div class="item">
						    <a href="under-implementation.php">
						        <img src="admin/db/imp_images/<?php echo $row_core_value['under_imp_img'];?>" alt="Owl Image">
						    </a>
						  </div>
			              <?php
			              }
			              }
			              else
			              {
			              ?>
			              <div class="col-xs-12">
			              <div class="alert alert-warning">
			              <span class="glyphicon glyphicon-info-sign"></span> &nbsp; No Data Found ...
			              </div>
			              </div>
			              <?php
			              }

			              ?>
						
					</div>
					<!-- //End -->
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>
	<!--//what-->

	<!-- Footer-start-here -->
	<?php include 'footer.php'; ?>
	<!-- //Footer-end-here -->
	<?php   
}else{
    ?>


<?php
 echo "<script>window.location.href='index.php'</script>";
}
?>	

	<script type="text/javascript" src="js/jquery-2.2.3.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.js"></script>
	<script>
		$('ul.dropdown-menu li').hover(function () {
			$(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
		}, function () {
			$(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
		});
	</script>

	<!-- js -->
	<!-- Smooth-Scrolling-JavaScript -->
	<script type="text/javascript" src="js/easing.js"></script>
	<script type="text/javascript" src="js/move-top.js"></script>
	<script type="text/javascript">
		jQuery(document).ready(function ($) {
			$(".scroll, .navbar li a, .footer li a").click(function (event) {
				$('html,body').animate({
					scrollTop: $(this.hash).offset().top
				}, 1000);
			});
		});
	</script>
	<!-- //Smooth-Scrolling-JavaScript -->
	<script type="text/javascript">
		$(document).ready(function () {
			/*
									var defaults = {
							  			containerID: 'toTop', // fading element id
										containerHoverID: 'toTopHover', // fading element hover id
										scrollSpeed: 1200,
										easingType: 'linear' 
							 		};
									*/

			$().UItoTop({
				easingType: 'easeOutQuart'
			});

		});
	</script>
	<a href="#home" class="scroll" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>

	<!-- owl-carousel -->
	<script type="text/javascript" src="js/jquery-2.2.4.min.js"></script>
	<script type="text/javascript" src="js/owl-carousel.js"></script>
	<!-- //End -->
	<!-- title-underline -->
	<script type="text/javascript">
		jQuery("h1").fitText(1.2);
	</script>
	<!-- //End -->
	
	<!-- jQuery-Photo-filter-lightbox-Gallery-plugin -->
	<script type="text/javascript" src="js/jquery-1.7.2.js"></script>
	<script src="js/jquery.quicksand.js" type="text/javascript"></script>
	<script src="js/script.js" type="text/javascript"></script>
	<script src="js/jquery.prettyPhoto.js" type="text/javascript"></script>
	<!-- //jQuery-Photo-filter-lightbox-Gallery-plugin -->

	<!-- Slider -->
	<script>
    // The Slideshow class.
class Slideshow {
    constructor(el) {
        
        this.DOM = {el: el};
      
        this.config = {
          slideshow: {
            delay: 3000,
            pagination: {
              duration: 3,
            }
          }
        };
        
        // Set the slideshow
        this.init();
      
    }
    init() {
      
      var self = this;
      
      // Charmed title
      this.DOM.slideTitle = this.DOM.el.querySelectorAll('.slide-title');
      this.DOM.slideTitle.forEach((slideTitle) => {
        charming(slideTitle);
      });
      
      // Set the slider
      this.slideshow = new Swiper (this.DOM.el, {
          
          loop: true,
          autoplay: {
            delay: this.config.slideshow.delay,
            disableOnInteraction: false,
          },
          speed: 500,
          preloadImages: true,
          updateOnImagesReady: true,
          
          // lazy: true,
          // preloadImages: false,

          pagination: {
            el: '.slideshow-pagination',
            clickable: true,
            bulletClass: 'slideshow-pagination-item',
            bulletActiveClass: 'active',
            clickableClass: 'slideshow-pagination-clickable',
            modifierClass: 'slideshow-pagination-',
            renderBullet: function (index, className) {
              
              var slideIndex = index,
                  number = (index <= 8) ? '0' + (slideIndex + 1) : (slideIndex + 1);
              
              var paginationItem = '<span class="slideshow-pagination-item">';
              paginationItem += '<span class="pagination-number">' + number + '</span>';
              paginationItem = (index <= 8) ? paginationItem + '<span class="pagination-separator"><span class="pagination-separator-loader"></span></span>' : paginationItem;
              paginationItem += '</span>';
            
              return paginationItem;
              
            },
          },

          // Navigation arrows
          navigation: {
            nextEl: '.slideshow-navigation-button.next',
            prevEl: '.slideshow-navigation-button.prev',
          },

          // And if we need scrollbar
          scrollbar: {
            el: '.swiper-scrollbar',
          },
        
          on: {
            init: function() {
              self.animate('next');
            },
          }
        
        });
      
        // Init/Bind events.
        this.initEvents();
        
    }
    initEvents() {
        
        this.slideshow.on('paginationUpdate', (swiper, paginationEl) => this.animatePagination(swiper, paginationEl));
        //this.slideshow.on('paginationRender', (swiper, paginationEl) => this.animatePagination());

        this.slideshow.on('slideNextTransitionStart', () => this.animate('next'));
        
        this.slideshow.on('slidePrevTransitionStart', () => this.animate('prev'));
            
    }
    animate(direction = 'next') {
      
        // Get the active slide
        this.DOM.activeSlide = this.DOM.el.querySelector('.swiper-slide-active'),
        this.DOM.activeSlideImg = this.DOM.activeSlide.querySelector('.slide-image'),
        this.DOM.activeSlideTitle = this.DOM.activeSlide.querySelector('.slide-title'),
        this.DOM.activeSlideTitleLetters = this.DOM.activeSlideTitle.querySelectorAll('span');
      
        // Reverse if prev  
        this.DOM.activeSlideTitleLetters = direction === "next" ? this.DOM.activeSlideTitleLetters : [].slice.call(this.DOM.activeSlideTitleLetters).reverse();
      
        // Get old slide
        this.DOM.oldSlide = direction === "next" ? this.DOM.el.querySelector('.swiper-slide-prev') : this.DOM.el.querySelector('.swiper-slide-next');
        if (this.DOM.oldSlide) {
          // Get parts
          this.DOM.oldSlideTitle = this.DOM.oldSlide.querySelector('.slide-title'),
          this.DOM.oldSlideTitleLetters = this.DOM.oldSlideTitle.querySelectorAll('span'); 
          // Animate
          this.DOM.oldSlideTitleLetters.forEach((letter,pos) => {
            TweenMax.to(letter, .3, {
              ease: Quart.easeIn,
              delay: (this.DOM.oldSlideTitleLetters.length-pos-1)*.04,
              y: '50%',
              opacity: 0
            });
          });
        }
      
        // Animate title
        this.DOM.activeSlideTitleLetters.forEach((letter,pos) => {
					TweenMax.to(letter, .6, {
						ease: Back.easeOut,
						delay: pos*.05,
						startAt: {y: '50%', opacity: 0},
						y: '0%',
						opacity: 1
					});
				});
      
        // Animate background
        TweenMax.to(this.DOM.activeSlideImg, 1.5, {
            ease: Expo.easeOut,
            startAt: {x: direction === 'next' ? 200 : -200},
            x: 0,
        });
      
        //this.animatePagination()
    
    }
    animatePagination(swiper, paginationEl) {
            
      // Animate pagination
      this.DOM.paginationItemsLoader = paginationEl.querySelectorAll('.pagination-separator-loader');
      this.DOM.activePaginationItem = paginationEl.querySelector('.slideshow-pagination-item.active');
      this.DOM.activePaginationItemLoader = this.DOM.activePaginationItem.querySelector('.pagination-separator-loader');
      
      console.log(swiper.pagination);
      // console.log(swiper.activeIndex);
      
      // Reset and animate
        TweenMax.set(this.DOM.paginationItemsLoader, {scaleX: 0});
        TweenMax.to(this.DOM.activePaginationItemLoader, this.config.slideshow.pagination.duration, {
          startAt: {scaleX: 0},
          scaleX: 1,
        });
      
      
    }
    
}
const slideshow = new Slideshow(document.querySelector('.slideshow'));
</script>
<!-- //End -->

<!-- Page-redirect -->
<script>
    function myFunction1() {
      location.replace("social-responsibility.php");
    }
</script>
<script>
    function myFunction2() {
      location.replace("management.php");
    }
</script>
<!-- //End -->

<!-- Tooltip -->
<script>
	$(function () {
	  $('[data-toggle="tooltip"]').tooltip()
	})
</script>
<!-- //End -->

</body>
</html>