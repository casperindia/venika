<?php
include 'dbconfig.php';
$email = $_SESSION['email'];

$get_login =$DB_con->prepare(" select * from login WHERE email = '$email' ");
$get_login->execute();
$login = $get_login->fetch();

$get_about_about_us =$DB_con->prepare(" select * from about_us WHERE about_id = '1'");
$get_about_about_us->execute();
$about_about_us = $get_about_about_us->fetch();
?>
<!DOCTYPE html>
<html>

<head>
	<title>Venika | About Us</title>
	<!--/tags -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="Conceit Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
	<script type="application/x-javascript">
		addEventListener("load", function () {
			setTimeout(hideURLbar, 0);
		}, false);

		function hideURLbar() {
			window.scrollTo(0, 1);
		}
	</script>
	<!--//tags -->
	<link rel="shortcut icon" href="images/home/venika-icon.png"/>
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.7/css/bootstrap-dialog.min.css">
	<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
	<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
	<link href="css/custom.css" rel="stylesheet" type="text/css" media="all" />
	<link href="css/team.css" rel="stylesheet" type="text/css" media="all" />
	<link href="css/font-awesome.css" rel="stylesheet">
	<!-- //for bootstrap working -->
	<link href="//fonts.googleapis.com/css?family=Raleway:100,100i,200,300,300i,400,400i,500,500i,600,600i,700,800" rel="stylesheet">
	<link href="//fonts.googleapis.com/css?family=Source+Sans+Pro:300,300i,400,400i,600,600i,700" rel="stylesheet">
	<!-- tab-&-img-modal-popup -->
	<link rel="stylesheet" href="w3css/4/w3.css">
	<!-- //End -->
</head>

<body style="text-align: justify;">
	<!-- header-top -->
	<div class="top_header" id="home">
		<!-- Fixed navbar -->
		<nav class="navbar navbar-default navbar-fixed-top">
			<div class="nav_top_fx_w3ls_agileinfo">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false"
					    aria-controls="navbar">
				        <span class="sr-only">Toggle navigation</span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				    </button>
					<div class="logo-w3layouts-agileits">
						<h1>
							<a class="navbar-brand" href="index.php">
								<!-- <i class="fa fa-clone" aria-hidden="true"></i> Conceit <span class="desc">For your Business</span> -->
								<img src="images/home/venika-logo-head.png" alt="" class="img-responsive">
							</a>
						</h1>
					</div>
				</div>
				<div id="navbar" class="navbar-collapse collapse">
					<div class="nav_right_top">
						<ul class="nav navbar-nav">
							<li><a class="nav-link" href="home.php">Home</a></li>
							<li class="active"><a class="nav-link" href="about.php">About Us</a></li>
							<li><a class="nav-link" href="management.php">Management</a></li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Projects <span class="caret"></span></a>
								<ul class="dropdown-menu">
									<li><a href="under-implementation.php">UNDER IMPLEMENTATION</a></li>
									<li><a href="under-construction.php">UNDER CONSTRUCTION</a></li>
									<li><a href="commissioned.php">COMMISSIONED</a></li>
								</ul>
							</li>
							<li><a class="nav-link" href="social-responsibility.php">Social Responsibility</a></li>
							<li><a class="nav-link" href="contact.php">Contact</a></li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user"></i>    <?php echo $login['name']; ?> <span class="caret"></span></a>
								<ul class="dropdown-menu">
									<li><a href="logout.php">Logout</a></li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
				<!--/.nav-collapse -->
			</div>
		</nav>
	</div>
	<!-- //End -->

	<!--/banner_info-->
	<div class="banner_inner_con"> </div>
	<div class="services-breadcrumb">
		<div class="inner_breadcrumb">
			<ul class="short">
				<li><a href="home.php">Home</a><span>|</span></li>
				<li>About</li>
			</ul>
		</div>
	</div>
	<!--//banner_info-->
    
	<!--/ab-->
	<div class="banner_bottom">
		<div class="container">
			<div class="title-underline">
				<h3 class="tittle-w3ls"><?php echo $about_about_us['about_title_one']; ?></h3>
			</div>
			<div class="inner_sec_info_wthree_agile">
				<div class="help_full help-full-img-pop">

					<div class="col-md-6 banner_bottom_grid help">
						<!-- w3.css-img-popup -->
						<img src="admin/db/about_image/<?php echo $about_about_us['about_image']; ?>" class="img-responsive w3-hover-opacity" style="cursor:zoom-in" onclick="document.getElementById('modal50').style.display='block'">
						<div id="modal50" class="w3-modal w3-modal-img-pop" onclick="this.style.display='none'">
						    <span class="w3-button-img-pop w3-hover-red w3-xlarge-img-pop w3-display-topright-img-pop">&times;</span>
						    <div class="w3-modal-content-img-pop w3-animate-zoom">
						      	<!-- <img src="admin/db/image/<?php echo $about_about_us['about_image']; ?>" class="img-responsive"> -->
						    </div>
						</div>
						<!-- //End -->

						<!-- <img src="images/ab.png" class="img-responsive w3-hover-opacity" onclick="document.getElementById('modal01').style.display='block'"> -->
					</div>
					<div class="col-md-6 banner_bottom_left about-content-div">
						<h4><?php echo $about_about_us['about_title_two']; ?></h4>
						<div class="scrollbar card" id="style-4">
							<div class="force-overflow">
								<p style="margin-bottom: 0.5em;"><?php echo $about_about_us['about_desc']; ?></p>
							</div>
						</div>
						<!-- <div class="ab_button">
							<a class="btn btn-primary btn-lg hvr-underline-from-left" href="#" role="button">Read More </a>
						</div> -->
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<?php
              $stmt_leadership = $DB_con->prepare('SELECT * FROM about_subdetails where data_delete = 0 ORDER BY about_sub_id ASC limit 4');
              $stmt_leadership->execute();

              if($stmt_leadership->rowCount() > 0)
              {
              while($leadership=$stmt_leadership->fetch(PDO::FETCH_ASSOC))
              {
              extract($leadership);
              ?>
            
              
			<div class="news-main">
				<div class="col-md-12 banner_bottom_left">
					<div class="banner_bottom_pos card">
						<div class="banner_bottom_pos_grid">
							<div class="col-xs-2 banner_bottom_grid_left vision-icon-div">
								<div class="banner_bottom_grid_left_grid">
									<span class="fa fa-tint" aria-hidden="true"></span>
								</div>
							</div>
							<div class="col-xs-10 banner_bottom_grid_right vision-content-div">
								<h4><?php echo $leadership['about_sub_title']; ?></h4>
								<p><?php echo $leadership['about_sub_desc']; ?></p>
							</div>
							<div class="clearfix"> </div>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		
	<?php
              }
              }
              else
              {
              ?>
              	<div class="col-xs-12">
              		<div class="alert alert-warning">
              			<span class="glyphicon glyphicon-info-sign"></span> &nbsp; No Data Found ...
              		</div>
              	</div>
              <?php
              }

              ?>
              </div>
	</div>
	<!--//ab-->
	
	<!-- Footer-start-here -->
	<?php include 'footer.php'; ?>
	<!-- //Footer-end-here -->
	
	<!-- js -->
	<script type="text/javascript" src="js/jquery-2.2.3.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.js"></script>
	<script>
		$('ul.dropdown-menu li').hover(function () {
			$(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
		}, function () {
			$(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
		});
	</script>
	<!-- start-smoth-scrolling -->
	<script type="text/javascript" src="js/move-top.js"></script>
	<script type="text/javascript" src="js/easing.js"></script>
	<script type="text/javascript">
		jQuery(document).ready(function ($) {
			$(".scroll").click(function (event) {
				event.preventDefault();
				$('html,body').animate({
					scrollTop: $(this.hash).offset().top
				}, 900);
			});
		});
	</script>
	<!-- start-smoth-scrolling -->
	<a href="#home" class="scroll" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>

	<!-- tab-modal-popup -->
	<script type="text/javascript">
		document.getElementsByClassName("tablink")[0].click();
		function openCity(evt, cityName) {
			var i, x, tablinks;
			x = document.getElementsByClassName("city");
			for (i = 0; i < x.length; i++) {
				x[i].style.display = "none";
			}
			tablinks = document.getElementsByClassName("tablink");
			for (i = 0; i < x.length; i++) {
				tablinks[i].classList.remove("w3-light-grey");
			}
			document.getElementById(cityName).style.display = "block";
			evt.currentTarget.classList.add("w3-light-grey");
		}
	</script>
	<!-- //End -->
</body>
</html>