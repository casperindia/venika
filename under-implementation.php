<?php
include 'dbconfig.php';
$email = $_SESSION['email'];

$get_login =$DB_con->prepare(" select * from login WHERE email = '$email' ");
$get_login->execute();
$login = $get_login->fetch();
?>
<!DOCTYPE html>
<html>

<head>
	<title>Venika | Under Implementation</title>

	<!--/tags -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="Conceit Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
	<script type="application/x-javascript">
		addEventListener("load", function () {
			setTimeout(hideURLbar, 0);
		}, false);

		function hideURLbar() {
			window.scrollTo(0, 1);
		}
	</script>
	<!--//tags -->
	<link rel="shortcut icon" href="images/home/venika-icon.png"/>
	<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
	<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
	<link href="css/custom.css" rel="stylesheet" type="text/css" media="all" />
	<link href="css/prettyPhoto.css" rel="stylesheet" type="text/css" />
	<link href="css/font-awesome.css" rel="stylesheet">
	<!-- //for bootstrap working -->
	<link href="//fonts.googleapis.com/css?family=Raleway:100,100i,200,300,300i,400,400i,500,500i,600,600i,700,800" rel="stylesheet">
	<link href="//fonts.googleapis.com/css?family=Source+Sans+Pro:300,300i,400,400i,600,600i,700" rel="stylesheet">
</head>

<body style="text-align: justify;">
	<!-- header-top -->
	<div class="top_header" id="home">
		<!-- Fixed navbar -->
		<nav class="navbar navbar-default navbar-fixed-top" style="border-bottom: 2px solid #118eaf;">
			<div class="nav_top_fx_w3ls_agileinfo">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false"
					    aria-controls="navbar">
				        <span class="sr-only">Toggle navigation</span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				    </button>
					<div class="logo-w3layouts-agileits">
						<h1>
							<a class="navbar-brand" href="index.php">
								<!-- <i class="fa fa-clone" aria-hidden="true"></i> Conceit <span class="desc">For your Business</span> -->
								<img src="images/home/venika-logo-head.png" alt="" class="img-responsive">
							</a>
						</h1>
					</div>
				</div>
				<div id="navbar" class="navbar-collapse collapse">
					<div class="nav_right_top">
						<ul class="nav navbar-nav">
							<li><a href="index.php">Home</a></li>
							<li><a href="about.php">About Us</a></li>
							<li><a href="management.php">Management</a></li>
							<li class="dropdown active">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Projects <span class="caret"></span></a>
								<ul class="dropdown-menu">
									<li class="active"><a href="under-implementation.php">UNDER IMPLEMENTATION</a></li>
									<li><a href="under-construction.php">UNDER CONSTRUCTION</a></li>
									<li><a href="commissioned.php">COMMISSIONED</a></li>
								</ul>
							</li>
							<li><a href="social-responsibility.php">Social Responsibility</a></li>
							<li><a href="contact.php">Contact</a></li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user"></i>    <?php echo $login['name']; ?> <span class="caret"></span></a>
								<ul class="dropdown-menu">
									<li><a href="logout.php">Logout</a></li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
				<!--/.nav-collapse -->
			</div>
		</nav>
	</div>
	<!-- //End -->

	<!--/banner_info-->
	<div class="banner_inner_con"> </div>
	<div class="services-breadcrumb">
		<div class="inner_breadcrumb">
			<ul class="short">
				<li><a href="index.php">Home</a><span>|</span></li>
				<li>Under Implementation</li>
			</ul>
		</div>
	</div>
	<!--//banner_info-->

	<!--/projects-->
	<div class="banner_bottom proj">
		<div class="wrap_view">
			<div class="title-underline">
				<h3 class="tittle-w3ls">Under Implementation</h3>
			</div>
			<div class="inner_sec">
				<!-- <ul class="portfolio-categ filter">
					<li class="port-filter all active">
						<a href="#">All</a>
					</li>
					<li class="cat-item-1">
						<a href="#" title="Category 1">Category 1</a>
					</li>
					<li class="cat-item-2">
						<a href="#" title="Category 2">Category 2</a>
					</li>
					<li class="cat-item-3">
						<a href="#" title="Category 3">Category 3</a>
					</li>
					<li class="cat-item-4">
						<a href="#" title="Category 4">Category 4</a>
					</li>
				</ul> -->
				<ul class="portfolio-area ui-gap">
					<!-- <li class="portfolio-item2" data-id="id-0" data-type="cat-item-4">
						<div class="ui-border">
							<span class="image-block img-hover">
								<a class="image-zoom" href="images/home/under-implementation1.jpg" rel="prettyPhoto[gallery]">
									<img src="images/home/under-implementation1.jpg" class="img-responsive" alt="Conceit">
									<div class="port-info ui-para">
										<h5>Mahan SHP II</h5>
										<p><b>Capacity :</b> 24.75 MW</p>
										<p><b>Location :</b> Mahan River</p>
										<p><b>State :</b> Chhattisgarh</p>
									</div>
								</a>
							</span>
						</div>
					</li> -->
			  <?php
              $stmt_core_value = $DB_con->prepare('SELECT * FROM under_implementation where data_delete = 0 ORDER BY under_imp_id desc limit 12');
              $stmt_core_value->execute();

              if($stmt_core_value->rowCount() > 0)
              {
              while($row_core_value=$stmt_core_value->fetch(PDO::FETCH_ASSOC))
              {
              extract($row_core_value);
              ?>
               <li class="portfolio-item2" data-id="id-0" data-type="cat-item-4">
				 <div class="ui-border">
				  <span class="image-block img-hover">
				  <a class="image-zoom" href="images/home/under-implementation1.jpg" rel="prettyPhoto[gallery]">
               	   <img src="admin/db/imp_images/<?php echo $row_core_value['under_imp_img'];?>" class="img-responsive" alt="Conceit">
	               	  <div class="port-info ui-para">
					     <h5><?php echo $row_core_value['under_imp_name']; ?></h5>
					     <p><b>Capacity :</b> <?php echo $row_core_value['under_imp_capacity']; ?></p>
					     <p><b>Location :</b> <?php echo $row_core_value['under_imp_location']; ?></p>
					     <p><b>State :</b> <?php echo $row_core_value['under_imp_state']; ?></p>
					  </div>
			     </a>
				</span>
				</div>
			  </li>
              <?php
              }
              }
              else
              {
              ?>
              <div class="col-xs-12">
              <div class="alert alert-warning">
              <span class="glyphicon glyphicon-info-sign"></span> &nbsp; No Data Found ...
              </div>
              </div>
              <?php
              }

              ?>
					<!-- <li class="portfolio-item2" data-id="id-1" data-type="cat-item-2">
						<div class="ui-border">
							<span class="image-block">
								<a class="image-zoom" href="images/home/under-implementation2.jpg" rel="prettyPhoto[gallery]">
									<img src="images/home/under-implementation2.jpg" class="img-responsive" alt="Conceit">
									<div class="port-info ui-para">
										<h5>Mahan SHP V</h5>
										<p><b>Capacity :</b> 13 MW</p>
										<p><b>Location :</b> Mahan River</p>
										<p><b>State :</b> Chhattisgarh</p>
									</div>
								</a>
							</span>
						</div>
					</li>
					<li class="portfolio-item2" data-id="id-2" data-type="cat-item-1">
						<div class="ui-border">
							<span class="image-block">
								<a class="image-zoom" href="images/home/under-implementation3.jpg" rel="prettyPhoto[gallery]">
									<img src="images/home/under-implementation3.jpg" class="img-responsive" alt="Conceit">
									<div class="port-info ui-para">
										<h5>Kerlapal SHP</h5>
										<p><b>Capacity :</b> 24.75 MW</p>
										<p><b>Location :</b> Sabari River</p>
										<p><b>State :</b> Chhattisgarh</p>
									</div>
								</a>
							</span>
						</div>
					</li>
					<li class="portfolio-item2" data-id="id-3" data-type="cat-item-4">
						<div class="ui-border">
							<span class="image-block">
								<a class="image-zoom" href="images/home/under-implementation4.jpg" rel="prettyPhoto[gallery]">
									<img src="images/home/under-implementation4.jpg" class="img-responsive" alt="Conceit">
									<div class="port-info ui-para">
										<h5>Dubbatota SHP</h5>
										<p><b>Capacity :</b> 24.75 MW</p>
										<p><b>Location :</b> Sabari River</p>
										<p><b>State :</b> Chhattisgarh</p>
									</div>
								</a>
							</span>
						</div>
					</li>
					<li class="portfolio-item2" data-id="id-4" data-type="cat-item-3">
						<div class="ui-border">
							<span class="image-block">
								<a class="image-zoom" href="images/home/under-implementation5.jpg" rel="prettyPhoto[gallery]">
									<img src="images/home/under-implementation5.jpg" class="img-responsive" alt="Conceit">
									<div class="port-info ui-para">
										<h5>Dornapalli SHP</h5>
										<p><b>Capacity :</b> 24.75 MW</p>
										<p><b>Location :</b> Sabari River</p>
										<p><b>State :</b> Chhattisgarh</p>
									</div>
								</a>
							</span>
						</div>
					</li>
					<li class="portfolio-item2" data-id="id-5" data-type="cat-item-2">
						<div class="ui-border">
							<span class="image-block">
								<a class="image-zoom" href="images/home/under-implementation6.jpg" rel="prettyPhoto[gallery]">
									<img src="images/home/under-implementation6.jpg" class="img-responsive" alt="Conceit">
									<div class="port-info ui-para">
										<h5>Peddakurti SHP</h5>
										<p><b>Capacity :</b> 24.75 MW</p>
										<p><b>Location :</b> Sabari River</p>
										<p><b>State :</b> Chhattisgarh</p>
									</div>
								</a>
							</span>
						</div>
					</li>
					<li class="portfolio-item2" data-id="id-5" data-type="cat-item-2">
						<div class="ui-border">
							<span class="image-block">
								<a class="image-zoom" href="images/home/under-implementation7.jpg" rel="prettyPhoto[gallery]">
									<img src="images/home/under-implementation7.jpg" class="img-responsive" alt="Conceit">
									<div class="port-info ui-para">
										<h5>Birla SHP</h5>
										<p><b>Capacity :</b> 24.75 MW</p>
										<p><b>Location :</b> Sabari River</p>
										<p><b>State :</b> Chhattisgarh</p>
									</div>
								</a>
							</span>
						</div>
					</li>
					<li class="portfolio-item2" data-id="id-6" data-type="cat-item-1">
						<div class="ui-border">
							<span class="image-block">
								<a class="image-zoom" href="images/home/under-implementation8.jpg" rel="prettyPhoto[gallery]">
									<img src="images/home/under-implementation8.jpg" class="img-responsive" alt="Conceit">
							      	<div class="port-info ui-para">
										<h5>Chikka SHP</h5>
										<p><b>Capacity :</b> 24.75 MW</p>
										<p><b>Location :</b> Kaveri River</p>
										<p><b>State :</b> Karnataka</p>
									</div>
								</a>
							</span>
						</div>
					</li>
					<li class="portfolio-item2" data-id="id-7" data-type="cat-item-1">
						<div class="ui-border">
							<span class="image-block">
								<a class="image-zoom" href="images/home/under-implementation9.jpg" rel="prettyPhoto[gallery]">
									<img src="images/home/under-implementation9.jpg" class="img-responsive" alt="Conceit">
									<div class="port-info ui-para">
										<h5>Ganalu SHP</h5>
										<p><b>Capacity :</b> 4 MW</p>
										<p><b>Location :</b> Shimsha River</p>
										<p><b>State :</b> Karnataka</p>
									</div>
								</a>
							</span>
						</div>
					</li -->
					<div class="clearfix"></div>
				</ul>
				<!--end portfolio-area -->
			</div>
		</div>
	</div>
	<!--//projects-->
	
	<!-- Footer-start-here -->
	<?php include 'footer.php'; ?>
	<!-- //Footer-end-here -->

	<script type="text/javascript" src="js/jquery-2.2.3.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.js"></script>
	<script>
		$('ul.dropdown-menu li').hover(function () {
			$(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
		}, function () {
			$(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
		});
	</script>

	<!-- js -->
	<!-- Smooth-Scrolling-JavaScript -->
	<script type="text/javascript" src="js/easing.js"></script>
	<script type="text/javascript" src="js/move-top.js"></script>
	<script type="text/javascript">
		jQuery(document).ready(function ($) {
			$(".scroll, .navbar li a, .footer li a").click(function (event) {
				$('html,body').animate({
					scrollTop: $(this.hash).offset().top
				}, 1000);
			});
		});
	</script>
	<!-- //Smooth-Scrolling-JavaScript -->
	<script type="text/javascript">
		$(document).ready(function () {
			/*
									var defaults = {
							  			containerID: 'toTop', // fading element id
										containerHoverID: 'toTopHover', // fading element hover id
										scrollSpeed: 1200,
										easingType: 'linear' 
							 		};
									*/

			$().UItoTop({
				easingType: 'easeOutQuart'
			});

		});
	</script>
	<a href="#home" class="scroll" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
	<!-- jQuery-Photo-filter-lightbox-Gallery-plugin -->
	<script type="text/javascript" src="js/jquery-1.7.2.js"></script>
	<script src="js/jquery.quicksand.js" type="text/javascript"></script>
	<script src="js/script.js" type="text/javascript"></script>
	<script src="js/jquery.prettyPhoto.js" type="text/javascript"></script>
	<!-- //jQuery-Photo-filter-lightbox-Gallery-plugin -->

</body>

</html>