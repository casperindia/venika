<?php
include('class.password.php');
class User extends Password{
    private $db;
	function __construct($db){
		parent::__construct();
		$this->_db = $db;
	}
	public function is_logged_in(){
		if(isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true){
			return true;
		}
	}
	private function get_user_hash($username){
		try {
			$stmt = $this->_db->prepare('SELECT * FROM account_user WHERE admin_email = :username');
			$stmt->execute(array('username' => $username));
			return $stmt->fetch();
		} catch(PDOException $e) {
		    echo '<p class="error" style="color:red;">'.$e->getMessage().'</p>';
		}
	}
	public function login($username,$password){
		$user = $this->get_user_hash($username);
		if(($password == $user['admin_pass'])){
		    $_SESSION['loggedin'] = true;
		    $_SESSION['admin_id'] = $user['admin_id'];
		    $_SESSION['admin_email'] = $user['admin_email'];
		    return true;
		}
	}
	public function logout(){
		session_destroy();
	}
}
?>
