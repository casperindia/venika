<!-- 
Developer : Rehna PV
Company Name : CasperIndia
-->
<?php
require_once 'db/dbconfig.php';
$admin_email = $_SESSION['admin_email'];

$get_admin =$DB_con->prepare(" select * from account_user WHERE admin_email = '$admin_email'");
$get_admin->execute();
$admin = $get_admin->fetch();

if(isset($_SESSION['admin_email'])){ 

$get_banner_video =$DB_con->prepare('select * from banner_video where banner_id =1');
$get_banner_video->execute();
$banner_video_video = $get_banner_video->fetch();
$banner_video = $banner_video_video['banner_video'];
$banner_video_id = $banner_video_video['banner_video_id'];
$banner_id = $banner_video_video['banner_id'];
?>
<!DOCTYPE HTML>
<html>
<head>
<title>Venika | Banner Video</title>
<link rel="shortcut icon" href="../images/short_icon1.png"/>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Venika's mission is to provide clients and market place leaders with solutions and services that help them solve their business and talent problems. Our deep expertise is in the space of Business & Talent Consulting, Executive Search, Talent Management, Regulatory & Statutory Consulting and Talent process outsourcing. Our offerings include Recruitment Process Outsourcing (RPO), Temporary and Flexi Staffing, Contractor Placement and 
Payroll Management." />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>

<!-- Bootstrap Core CSS -->
<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />

<!-- Custom CSS -->
<link href="css/style.css" rel='stylesheet' type='text/css' />

<!-- font-awesome icons CSS -->
<link href="css/font-awesome.css" rel="stylesheet"> 
<!-- //font-awesome icons CSS -->

 <!-- side nav css file -->
 <link href='css/SidebarNav.min.css' media='all' rel='stylesheet' type='text/css'/>
 <!-- side nav css file -->
 
 <!-- js-->
<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/modernizr.custom.js"></script>

<!--webfonts-->
<link href="//fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i&amp;subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet">
<!--//webfonts--> 

<!-- Metis Menu -->
<script src="js/metisMenu.min.js"></script>
<script src="js/custom.js"></script>
<link href="css/custom.css" rel="stylesheet">
<!--//Metis Menu -->
<style>
.para{
	text-align:justify;
}
</style>
</head> 
<body class="cbp-spmenu-push">
	<div class="main-content">
	<div class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="cbp-spmenu-s1">
		<!--left-fixed -navigation-->
		<?php include 'menu.php'; ?>
	</div>
		<!--left-fixed -navigation-->
		
		<!-- Start Notification Count -->
		<?php include 'header.php'; ?>
		<!-- End Notification Count -->
		<!-- main content start-->
		<div id="page-wrapper">
			<div class="main-page">
				<div class="forms">
					<div class="row">
						<h3 class="title1"><a href="home_page.php"> Home</a> // Banner Video </h3>
						<div class="form-three widget-shadow">
							<form class="form-horizontal" enctype="multipart/form-data" id="insert_form" method="post">
								<h4>Video</h4>
								<br>
				                <div class="form-group">
									<label for="Emp_code" class="col-sm-2 control-label">Video Link</label>
									<div class="col-sm-8">
										<label class="control-label"><b><?php echo $banner_video; ?></b></label>
									</div>
								</div>
								<div class="form-group">
									<label for="Emp_code" class="col-sm-2 control-label">Video Id</label>
									<div class="col-sm-8">
										<label class="control-label"><b><?php echo $banner_video_id; ?></b></label>
									</div>
								</div>
								<div class="form-group">
									<label for="Emp_code" class="col-sm-2 control-label"></label>
									<div class="col-sm-10">
									<a href="edit_banner_video.php?banner_id=<?php echo $banner_id; ?>" class="btn btn-warning" >EDIT</a>
									<a href="home_page.php" class="btn btn-danger" >Go Back</a>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--footer-->
		<?php include 'footer.php'; ?>
        <!--//footer-->
	</div>
	<?php   
}else{
    ?>


<?php
 echo "<script>window.location.href='index.php'</script>";
}
?>
<!-- tooltip -->
	<script>
		$(document).ready(function(){
		    $('[data-toggle="popover"]').popover();  
		});
	</script>
	<script>
		$(document).ready(function(){
		    $('[data-toggle="tooltip"]').tooltip();   
		});
	</script>
	<!-- //tooltip -->
	<!-- side nav js -->
	<script src='js/SidebarNav.min.js' type='text/javascript'></script>
	<script>
      $('.sidebar-menu').SidebarNav()
    </script>
	<!-- //side nav js -->
	
	<!-- Classie --><!-- for toggle left push menu script -->
		<script src="js/classie.js"></script>
		<script>
			var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
				showLeftPush = document.getElementById( 'showLeftPush' ),
				body = document.body;
				
			showLeftPush.onclick = function() {
				classie.toggle( this, 'active' );
				classie.toggle( body, 'cbp-spmenu-push-toright' );
				classie.toggle( menuLeft, 'cbp-spmenu-open' );
				disableOther( 'showLeftPush' );
			};
			
			function disableOther( button ) {
				if( button !== 'showLeftPush' ) {
					classie.toggle( showLeftPush, 'disabled' );
				}
			}
		</script>
	<!-- //Classie --><!-- //for toggle left push menu script -->
	
	<!--scrolling js-->
	<script src="js/jquery.nicescroll.js"></script>
	<script src="js/scripts.js"></script>
	<!--//scrolling js-->
	
	<!-- Bootstrap Core JavaScript -->
   <script src="js/bootstrap.js"> </script>
   
</body>
</html>