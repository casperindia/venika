<!--
Developer : Rehna PV
Website : www.casperindia.com
-->

<?php
require_once 'db/dbconfig.php';
$admin_email = $_SESSION['admin_email'];

$get_admin =$DB_con->prepare(" select * from account_user WHERE admin_email = '$admin_email'");
$get_admin->execute();
$admin = $get_admin->fetch();

if(isset($_SESSION['admin_email'])){ 
 
 if(isset($_GET['social_img_id']) && !empty($_GET['social_img_id']))
 {
  $id = $_GET['social_img_id'];
  $stmt_edit = $DB_con->prepare('SELECT * FROM social_image WHERE social_img_id =:uid');
  $stmt_edit->execute(array(':uid'=>$id));
  $edit_row = $stmt_edit->fetch(PDO::FETCH_ASSOC);
  extract($edit_row);
 }
 else
 {
  header("Location: show_social_slide.php");
 }
 ?>
<!DOCTYPE HTML>
<html>
<head>
<title>Venika | Edit Social Slide</title>
<link rel="shortcut icon" href="../images/short_icon1.png"/>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Venika's mission is to provide clients and market place leaders with solutions and services that help them solve their business and talent problems. Our deep expertise is in the space of Business & Talent Consulting, Executive Search, Talent Management, Regulatory & Statutory Consulting and Talent process outsourcing. Our offerings include Recruitment Process Outsourcing (RPO), Temporary and Flexi Staffing, Contractor Placement and 
Payroll Management." />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>

<!-- Bootstrap Core CSS -->
<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />

<!-- Custom CSS -->
<link href="css/style.css" rel='stylesheet' type='text/css' />

<!-- font-awesome icons CSS -->
<link href="css/font-awesome.css" rel="stylesheet"> 
<!-- //font-awesome icons CSS -->

 <!-- side nav css file -->
 <link href='css/SidebarNav.min.css' media='all' rel='stylesheet' type='text/css'/>
 <!-- side nav css file -->
 
 <!-- js-->
<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/modernizr.custom.js"></script>

<!--webfonts-->
<link href="//fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i&amp;subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet">
<!--//webfonts--> 

<!-- Metis Menu -->
<script src="js/metisMenu.min.js"></script>
<script src="js/custom.js"></script>
<link href="css/custom.css" rel="stylesheet">
<!--//Metis Menu -->

</head> 
<body class="cbp-spmenu-push">
	<div class="main-content">
	<div class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="cbp-spmenu-s1">
		<!--left-fixed -navigation-->
		<?php include'menu.php'; ?>
	</div>
		<!--left-fixed -navigation-->
		<?php include'header.php'; ?>
		
		<!-- main content start-->
		<div id="page-wrapper">
			<div class="main-page">
				<div class="forms">
					<div class="row">
						<h3 class="title1"><a href="show_social_slide.php"> Social Slide Details</a> // Edit</h3>
						<div class="form-three widget-shadow">
							<form class="form-horizontal" enctype="multipart/form-data" id="update_form" method="post">
                               
								<div class="form-group">
									<label for="Occupation" class="col-sm-2 control-label">Social Image</label>
									<div class="col-sm-8">
                                        <img src="db/social_slide_images/<?php echo $social_img_image; ?>" style="height: 350px; width: 100%;">
                                        <p class="text-danger">**Image size should be (640 x 480)pixel only**</p>
                                        <input type="file" name="social_img_image" id="social_img_image" accept="image/*">
                                        <input type="hidden" class="form-control1" id="social_img_id" name="social_img_id" value="<?php echo $social_img_id; ?>">
									</div>
									<div class="col-sm-2">
									</div>
								
								</div>
								
								<div class="form-group">
									<label for="name" class="col-sm-2 control-label"></label>
									<div class="col-sm-8">
										<input type="submit" name="submit" class="btn btn-success submitBtn" value="SAVE"/>
										<a href="show_social_slide.php" class="btn btn-danger">GO BACK</a>
										<p class="statusMsg"></p>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--footer-->
		<?php include'footer.php'; ?>
        <!--//footer-->
	</div>
	<?php   
}else{
    ?>


<?php
 echo "<script>window.location.href='index.php'</script>";
}
?>
	<!-- side nav js -->
	<script src='js/SidebarNav.min.js' type='text/javascript'></script>
	<script>
      $('.sidebar-menu').SidebarNav()
    </script>
	<!-- //side nav js -->
	
	<!-- Classie --><!-- for toggle left push menu script -->
		<script src="js/classie.js"></script>
		<script>
			var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
				showLeftPush = document.getElementById( 'showLeftPush' ),
				body = document.body;
				
			showLeftPush.onclick = function() {
				classie.toggle( this, 'active' );
				classie.toggle( body, 'cbp-spmenu-push-toright' );
				classie.toggle( menuLeft, 'cbp-spmenu-open' );
				disableOther( 'showLeftPush' );
			};
			
			function disableOther( button ) {
				if( button !== 'showLeftPush' ) {
					classie.toggle( showLeftPush, 'disabled' );
				}
			}
		</script>
	<!-- //Classie --><!-- //for toggle left push menu script -->
<script>
$(document).ready(function(e){
    $("#update_form").on('submit', function(e){
        e.preventDefault();
        $.ajax({
            type: 'POST',
            url: 'db/edit_social_slide.php',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData:false,
            beforeSend: function(){
                $('.submitBtn').attr("disabled","disabled");
                $('#update_form').css("opacity",".5");
            },
            success: function(msg){
                $('.statusMsg').html('');
                if(msg == 'ok'){
                    $('#update_form')[0].reset();
                    $('.statusMsg').html('<span style="font-size:15px;color:#34A853"> Updated successfully. <img src="images/tick.gif" style="width: 30px; height: 30px;"></span>');
                    setTimeout(function () {
   						window.location.href= 'show_social_slide.php'; // the redirect goes here

					},1000);

                }else{
                    $('.statusMsg').html('<span style="font-size:15px;color:#EA4335">Some problem occurred, please try again.</span>');
                }
                $('#update_form').css("opacity","");
                $(".submitBtn").removeAttr("disabled");
            }
        });
    });
    
});
</script>
	<!--scrolling js-->
	<script src="js/jquery.nicescroll.js"></script>
	<script src="js/scripts.js"></script>
	<!--//scrolling js-->
	
	<!-- Bootstrap Core JavaScript -->
   <script src="js/bootstrap.js"> </script>
   
</body>
</html>