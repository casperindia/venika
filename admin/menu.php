		<aside class="sidebar-left">
      <nav class="navbar navbar-inverse">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".collapse" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            </button>
            <h1><a class="navbar-brand" href="index.php"><img src="images/venika_logo.png" style="height:73px; width: 130px;" ></a></h1>
          </div>
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" style="margin: 2em 0;">
            <ul class="sidebar-menu">
              <li class="header">MAIN NAVIGATION</li>
              <li class="treeview">
                <a href="index.php">
                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                </a>
              </li>
              <li class="treeview">
                <a href="home_page.php">
                <i class="fa fa-laptop"></i>
                <span>Home</span>
                <span class="label label-primary pull-right"></span>
                </a>
              </li>
              <li class="treeview">
                <a href="#">
                <i class="fa fa-briefcase" aria-hidden="true"></i>
                <span>About Us</span>
                <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                  <li><a href="show_aboutus.php"><i class="fa fa-angle-right"></i> About Us</a></li>
                  <li><a href="show_subdetails.php"><i class="fa fa-angle-right"></i>Sub Details</a></li>
                 </ul>
              </li>
              <li class="treeview">
                <a href="management.php">
                <i class="fa fa-object-group" aria-hidden="true"></i>
                <span>Management</span>
                <span class="label label-primary pull-right"></span>
                </a>
              </li>
              <li class="treeview">
                <a href="#">
                <i class="fa fa-briefcase" aria-hidden="true"></i>
                <span>Projects</span>
                <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                  <li><a href="show_implementation.php"><i class="fa fa-angle-right"></i> Under Implementation</a></li>
                  <li><a href="show_construction.php"><i class="fa fa-angle-right"></i> Under Construction</a></li>
                  <li><a href="show_commissioned.php"><i class="fa fa-angle-right"></i> Commissioned</a></li>
                 </ul>
              </li>
              <li class="treeview">
                <a href="social_responsibility.php">
                <i class="fa fa-hand-paper-o" aria-hidden="true"></i>
                <span>Social Responsibility</span>
                <span class="label label-primary pull-right"></span>
                </a>
              </li>
              
              <li class="treeview">
                <a href="contact.php">
                <i class="fa fa-envelope" aria-hidden="true"></i>
                <span>Contacts</span>
                <span class="label label-primary pull-right"></span>
                </a>
              </li>
              <li class="treeview">
                <a href="show_users.php">
                <i class="fa fa-unlock-alt" aria-hidden="true"></i>
                <span>Admin</span>
                <span class="label label-primary pull-right"></span>
                </a>
              </li>
            </ul>
          </div>
          <!-- /.navbar-collapse -->
      </nav>
    </aside>