<!--
Company : CasperIndia
Website : www.casperindia.com
-->

<?php
require_once 'db/dbconfig.php';
$admin_email = $_SESSION['admin_email'];

$get_admin =$DB_con->prepare(" select * from account_user WHERE admin_email = '$admin_email'");
$get_admin->execute();
$admin = $get_admin->fetch();

if(isset($_SESSION['admin_email'])){ 

 if(isset($_GET['under_construction_id']) && !empty($_GET['under_construction_id']))
 {
  $id = $_GET['under_construction_id'];
  $stmt_edit = $DB_con->prepare('SELECT * FROM under_construction WHERE under_construction_id =:uid');
  $stmt_edit->execute(array(':uid'=>$id));
  $edit_row = $stmt_edit->fetch(PDO::FETCH_ASSOC);
  extract($edit_row);
 }
 else
 {
  header("Location: show_construction.php");
 }
 ?>
<!DOCTYPE HTML>
<html>
<head>
<title>Venika | Edit Implementation</title>
<link rel="shortcut icon" href="../images/short_icon1.png"/>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Venika's mission is to provide clients and market place leaders with solutions and services that help them solve their business and talent problems. Our deep expertise is in the space of Business & Talent Consulting, Executive Search, Talent Management, Regulatory & Statutory Consulting and Talent process outsourcing. Our offerings include Recruitment Process Outsourcing (RPO), Temporary and Flexi Staffing, Contractor Placement and 
Payroll Management." />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>

<!-- Bootstrap Core CSS -->
<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />

<!-- Custom CSS -->
<link href="css/style.css" rel='stylesheet' type='text/css' />

<!-- font-awesome icons CSS -->
<link href="css/font-awesome.css" rel="stylesheet"> 
<!-- //font-awesome icons CSS -->

 <!-- side nav css file -->
 <link href='css/SidebarNav.min.css' media='all' rel='stylesheet' type='text/css'/>
 <!-- side nav css file -->
 
 <!-- js-->
<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/modernizr.custom.js"></script>

<!--webfonts-->
<link href="//fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i&amp;subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet">
<!--//webfonts--> 

<!-- Metis Menu -->
<script src="js/metisMenu.min.js"></script>
<script src="js/custom.js"></script>
<link href="css/custom.css" rel="stylesheet">
<!--//Metis Menu -->
<style type="text/css">
  #buf_icon{
    float: left;
      margin: 3px;
  }
</style>
</head> 
<body class="cbp-spmenu-push">
  <div class="main-content">
  <div class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="cbp-spmenu-s1">
    <!--left-fixed -navigation-->
    <?php include'menu.php'; ?>
  </div>
    <!--left-fixed -navigation-->
    <?php include'header.php'; ?>
    
    <!-- main content start-->
    <div id="page-wrapper">
      <div class="main-page">
        <div class="forms">
          <div class="row">
            <h3 class="title1"><a href="show_implementation.php"> Under Construction</a> // Edit</h3>
            <div class="form-three widget-shadow">
              <form class="form-horizontal" enctype="multipart/form-data" id="update_form" method="post" novalidate>
                <div class="form-group">
                  <label for="Occupation" class="col-sm-2 control-label">Project Name</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control1" id="under_construction_name" name="under_construction_name" value="<?php echo $under_construction_name; ?>" placeholder="Enter Project Name" required="">
                    <input type="hidden" id="under_construction_id" name="under_construction_id" value="<?php echo $under_construction_id; ?>">
                  </div>
                  <div class="col-sm-2">
                  </div>
                </div>
                <div class="form-group">
                  <label for="Occupation" class="col-sm-2 control-label">Capacity</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control1" id="under_construction_capacity" name="under_construction_capacity" value="<?php echo $under_construction_capacity; ?>" placeholder="Enter Capacity" required="">
                  </div>
                  <div class="col-sm-2">
                  </div>
                </div>
                <div class="form-group">
                  <label for="Occupation" class="col-sm-2 control-label">Location</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control1" id="under_construction_location" name="under_construction_location" value="<?php echo $under_construction_location; ?>" placeholder="Enter Location" required="">
                  </div>
                  <div class="col-sm-2">
                  </div>
                </div>
                <div class="form-group">
                  <label for="Occupation" class="col-sm-2 control-label">State</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control1" id="under_construction_state" name="under_construction_state" value="<?php echo $under_construction_state; ?>" placeholder="Enter State" required="">
                  </div>
                  <div class="col-sm-2">
                  </div>
                </div>
                   <div class="form-group">
                  <label for="Occupation" class="col-sm-2 control-label">Image</label>
                  <div class="col-sm-8">
                  <img src="db/con_image/<?php echo $under_construction_image; ?>" style="height: 200px; width: 200px;">
                  <p class="text-danger" style="font-size: 13px;"> **Upload image size : (950 x 600) pixcel**</p>
                  <input type="file" name="under_construction_image" id="under_construction_image" accept="image/*">
                  </div>
                  <div class="col-sm-2">
                  </div>
                </div>
                <div class="form-group">
                  <label for="name" class="col-sm-2 control-label"></label>
                  <div class="col-sm-8">
                  <input type="submit" name="submit" class="btn btn-success submitBtn" id="buf_icon" value="SAVE"/>
                    <a href="show_construction.php" class="btn btn-danger" id="buf_icon">GO BACK</a>
                    <p class="statusMsg" id="buf_icon"></p>
                  </div>
                </div>
                
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- text editor ? model -->
  <!-- Modal -->
  <div class="modal fade" id="myModal_texteditor" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Text Editing Cotrols</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-6">
              <p>
                &lt;strongblog&gt;Look just like this&lt;/strongblog&gt;
              </p>
            </div>
            <div class="col-md-6">
              <strong>Look just like this</strong>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <p>
                &lt;italicblog&gt;Look just like this&lt;/italicblog&gt;
              </p>
            </div>
            <div class="col-md-6">
              <i>Look just like this</i>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <p>
                &lt;underlineblog&gt;Look just like this&lt;/underlineblog&gt;
              </p>
            </div>
            <div class="col-md-6">
              <u>Look just like this</u>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <p>
                &lt;br /&gt; 
              </p>
            </div>
            <div class="col-md-6">
              <p>For Next Line</p>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <p>
                &lt;p&gt; &lt;p/&gt; 
              </p>
            </div>
            <div class="col-md-6">
              <p>For Next Para</p>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <p>
                &lt;q&gt; &lt;/q&gt;
              </p>
            </div>
            <div class="col-md-6">
              <p>" For Giving "</p>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <p>
                &lt;ul&gt;</br>
                &lt;li&gt; List One &lt;/li&gt;</br>
                &lt;li&gt; List two &lt;/li&gt;</br>
                &lt;li&gt; List three &lt;/li&gt;</br>
                &lt;/ul&gt;
              </p>
            </div>
            <div class="col-md-6">
              <ul>
          <li>List One</li>
          <li>List two</li>
          <li>List Three</li>
        </ul>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
<!-- //end -->
    <!--footer-->
    <?php include'footer.php'; ?>
        <!--//footer-->
  </div>
  <?php   
}else{
    ?>


<?php
 echo "<script>window.location.href='index.php'</script>";
}
?>
  <!-- side nav js -->
  <script src='js/SidebarNav.min.js' type='text/javascript'></script>
  <script>
      $('.sidebar-menu').SidebarNav()
    </script>
  <!-- //side nav js -->
  
  <!-- Classie --><!-- for toggle left push menu script -->
    <script src="js/classie.js"></script>
    <script>
      var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
        showLeftPush = document.getElementById( 'showLeftPush' ),
        body = document.body;
        
      showLeftPush.onclick = function() {
        classie.toggle( this, 'active' );
        classie.toggle( body, 'cbp-spmenu-push-toright' );
        classie.toggle( menuLeft, 'cbp-spmenu-open' );
        disableOther( 'showLeftPush' );
      };
      
      function disableOther( button ) {
        if( button !== 'showLeftPush' ) {
          classie.toggle( showLeftPush, 'disabled' );
        }
      }
    </script>
  <!-- //Classie --><!-- //for toggle left push menu script -->

<script>
$(document).ready(function(e){
    $("#update_form").on('submit', function(e){
        e.preventDefault();
        $.ajax({
            type: 'POST',
            url: 'db/edit_construction.php',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData:false,
            beforeSend: function(){
                $('.submitBtn').attr("disabled","disabled");
                $('#update_form').css("opacity",".5");
            },
            success: function(msg){
                $('.statusMsg').html('');
                if(msg == 'ok'){
                    $('#update_form')[0].reset();
                    $('.statusMsg').html('<span style="font-size:15px;color:#34A853"> Updated Successfully. <img src="images/tick.gif" style="width: 30px; height: 30px;"></span>');
                    setTimeout(function () {
              window.location.href= 'show_construction.php'; // the redirect goes here

          },1000);

                }else{
                    $('.statusMsg').html('<span style="font-size:18px;color:#EA4335">Some problem occurred, please try again.</span>');
                }
                $('#update_form').css("opacity","");
                $(".submitBtn").removeAttr("disabled");
            }
        });
    });
    
});
</script>
  <!--scrolling js-->
  <script src="js/jquery.nicescroll.js"></script>
  <script src="js/scripts.js"></script>
  <!--//scrolling js-->
  
  <!-- Bootstrap Core JavaScript -->
   <script src="js/bootstrap.js"> </script>
   
</body>
</html>