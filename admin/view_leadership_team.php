<!--
Company : CasperIndia
Website : www.casperindia.com
-->
<?php
require_once 'db/dbconfig.php';
$admin_email = $_SESSION['admin_email'];

$get_admin =$DB_con->prepare(" select * from account_user WHERE admin_email = '$admin_email'");
$get_admin->execute();
$admin = $get_admin->fetch();

if(isset($_SESSION['admin_email'])){ 

 if(isset($_GET['leadership_id']) && !empty($_GET['leadership_id']))
 {
  $id = $_GET['leadership_id'];
  $stmt_edit = $DB_con->prepare('SELECT * FROM  leadership WHERE leader_id =:uid');
  $stmt_edit->execute(array(':uid'=>$id));
  $edit_row = $stmt_edit->fetch(PDO::FETCH_ASSOC);
  extract($edit_row);
 }
 else
 {
  header("Location: show_leadership_team.php");
 }
 ?>
<!DOCTYPE HTML>
<html>
<head>
<title>Venika | View Venika</title>
<link rel="shortcut icon" href="../images/short_icon1.png"/>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Venika's mission is to provide clients and market place leaders with solutions and services that help them solve their business and talent problems. Our deep expertise is in the space of Business & Talent Consulting, Executive Search, Talent Management, Regulatory & Statutory Consulting and Talent process outsourcing. Our offerings include Recruitment Process Outsourcing (RPO), Temporary and Flexi Staffing, Contractor Placement and 
Payroll Management." />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>

<!-- Bootstrap Core CSS -->
<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />

<!-- Custom CSS -->
<link href="css/style.css" rel='stylesheet' type='text/css' />

<!-- font-awesome icons CSS -->
<link href="css/font-awesome.css" rel="stylesheet"> 
<!-- //font-awesome icons CSS -->

 <!-- side nav css file -->
 <link href='css/SidebarNav.min.css' media='all' rel='stylesheet' type='text/css'/>
 <!-- side nav css file -->
 
 <!-- js-->
<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/modernizr.custom.js"></script>

<!--webfonts-->
<link href="//fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i&amp;subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet">
<!--//webfonts--> 

<!-- Metis Menu -->
<script src="js/metisMenu.min.js"></script>
<script src="js/custom.js"></script>
<link href="css/custom.css" rel="stylesheet">
<!--//Metis Menu -->

</head> 
<body class="cbp-spmenu-push">
	<div class="main-content">
	<div class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="cbp-spmenu-s1">
		<!--left-fixed -navigation-->
		<?php include'menu.php'; ?>
	</div>
		<!--left-fixed -navigation-->
		<?php include'header.php'; ?>
		
		<!-- main content start-->
		<div id="page-wrapper">
			<div class="main-page">
				<div class="forms">
					<div class="row">
						<h3 class="title1"><a href="show_leadership_team.php"> Leadership Team</a> // View</h3>
						<div class="form-three widget-shadow">
							<form class="form-horizontal" enctype="multipart/form-data" id="update_form" method="post">
                                <div class="form-group">
									<label for="Occupation" class="col-sm-2 control-label"><b>Title</b></label>
									<div class="col-sm-8">
										<label class="control-label"><?php echo $leader_name; ?></label>
									</div>
								</div>
                                <div class="form-group">
									<label for="Occupation" class="col-sm-2 control-label"><b>Designation</b></label>
									<div class="col-sm-8">
										<label class="control-label"><?php echo $leader_des; ?></label>
									</div>
								</div>
								 <div class="form-group">
									<label for="Occupation" class="col-sm-2 control-label"><b>Description</b></label>
									<div class="col-sm-8">
										<label class="control-label"><?php echo $leader_desc; ?></label>
									</div>
								</div>
								<div class="form-group">
									<label for="Occupation" class="col-sm-2 control-label"><b>Role</b></label>
									<div class="col-sm-8">
										<label class="control-label"><?php echo $leader_role; ?></label>
									</div>
								</div>
								<div class="form-group">
									<label for="Occupation" class="col-sm-2 control-label"><b>Facebook Link</b></label>
									<div class="col-sm-8">
										<label class="control-label"><?php echo $f_link; ?></label>
									</div>
								</div>
								<div class="form-group">
									<label for="Occupation" class="col-sm-2 control-label"><b>Twiter Link</b></label>
									<div class="col-sm-8">
										<label class="control-label"><?php echo $t_link; ?></label>
									</div>
								</div>
                                <div class="form-group">
									<label for="Occupation" class="col-sm-2 control-label"><b>Image</b></label>
									<div class="col-sm-4">
										<label class="control-label"><img src="db/leader_image/<?php echo $leader_image; ?>" style="height:150px;width:200px;"></label>
									</div>
								</div>
                                <div class="form-group">
									<label for="Occupation" class="col-sm-2 control-label"><b>Created On</b></label>
									<div class="col-sm-8">
										<label class="control-label"><?php echo $created_on; ?></label>
									</div>
								</div>
                            </form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--footer-->
		<?php include'footer.php'; ?>
        <!--//footer-->
	</div>
	<?php   
}else{
    ?>


<?php
 echo "<script>window.location.href='index.php'</script>";
}
?>
	<!-- side nav js -->
	<script src='js/SidebarNav.min.js' type='text/javascript'></script>
	<script>
      $('.sidebar-menu').SidebarNav()
    </script>
	<!-- //side nav js -->
	
	<!-- Classie --><!-- for toggle left push menu script -->
		<script src="js/classie.js"></script>
		<script>
			var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
				showLeftPush = document.getElementById( 'showLeftPush' ),
				body = document.body;
				
			showLeftPush.onclick = function() {
				classie.toggle( this, 'active' );
				classie.toggle( body, 'cbp-spmenu-push-toright' );
				classie.toggle( menuLeft, 'cbp-spmenu-open' );
				disableOther( 'showLeftPush' );
			};
			
			function disableOther( button ) {
				if( button !== 'showLeftPush' ) {
					classie.toggle( showLeftPush, 'disabled' );
				}
			}
		</script>
	<!-- //Classie --><!-- //for toggle left push menu script -->
	<!--scrolling js-->
	<script src="js/jquery.nicescroll.js"></script>
	<script src="js/scripts.js"></script>
	<!--//scrolling js-->
	
	<!-- Bootstrap Core JavaScript -->
   <script src="js/bootstrap.js"> </script>
   
</body>
</html>