<?php
//include config
require_once('config/config.php');

//check if already logged in
if( $user->is_logged_in() ){ header('Location: home.php'); } 
?>
<!DOCTYPE HTML>
<html lang="zxx">
<head>
	<title>Venika :: Portal</title>
	<link rel="shortcut icon" href="images/logo_icon.png"/>
	<!-- Meta tag Keywords -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="Alicon Portal Login."
	/>
	<script>
		addEventListener("load", function () {
			setTimeout(hideURLbar, 0);
		}, false);

		function hideURLbar() {
			window.scrollTo(0, 1);
		}
	</script>
	
	<!-- Meta tag Keywords -->
	<!-- css files -->
	<link rel="stylesheet" href="css/css_login/style.css" type="text/css" media="all" />
	<!-- Style-CSS -->
	<link rel="stylesheet" href="css/css_login/font-awesome.css">
	<!-- Font-Awesome-Icons-CSS -->
	<!-- //css files -->
	<!-- online-fonts -->
	<link href="//fonts.googleapis.com/css?family=Tangerine:400,700" rel="stylesheet">
	<link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
	<!-- //online-fonts -->
</head>

<body class="admin-page">
	<!--//header-->
	<div class="main-content-agile" style="margin-top: 150px;">
		<div class="sub-main-w3">
			<img src="images/venika_logo.png" alt="" class="img-responsive" style="width: 200px;" />
			<!-- <h2>Welcome To Alicon</h2> -->
			<h2><p class="statusMsg"></p></h2>
			<!-- <h2><?php echo $_SESSION['sess_msg'];?></h2> -->
			<?php
				//process login form if submitted
				if(isset($_POST['submit']))
				{
					$username = trim($_POST['account_user']);
					$password = trim($_POST['account_password']);

					if($user->login($username,$password))
					{ 
					//logged in return to index page
						header('Location: index.php');
						exit;
					} 
					else
					{
						$message = '<p class="error" style="color:red;">Incurrect username or password</p>';
					}
				}//end if submit
				if(isset($message)){ echo $message; }
			?>
			<form class="form-horizontal" enctype="multipart/form-data" method="post">
				<div class="pom-agile">
					<span class="fa fa-user-o" aria-hidden="true"></span>
					<input placeholder="Username" name="account_user" class="user" id="account_user" type="text" required="">
				</div>
				<div class="pom-agile">
					<span class="fa fa-key" aria-hidden="true"></span>
					<input placeholder="Password" name="account_password" id="account_password" class="pass" type="password" required="">
				</div>
				
				<div class="right-w3l">
					<!--<input type="button" name="submit" onclick="loginFuncton()"  value="Login">-->
					<input type="submit" name="submit" value="Login">
					
				</div>
				<!-- <div class="forgot-grid">
					<div class="forgot">
						<a href="forget_password" style="color: #fff;">forgot password?</a>
					</div>
					<div class="clearfix"> </div>
				</div> -->
			</form>
		</div>
	</div>
	<!--//main-->
	<!--footer-->
	<?php include 'footer.php'; ?>
	
</body>

</html>
