<!-- 
Developer : Rehna PV
Company Name : CasperIndia
-->
<?php
require_once 'db/dbconfig.php';
$admin_email = $_SESSION['admin_email'];

$get_admin =$DB_con->prepare(" select * from account_user WHERE admin_email = '$admin_email'");
$get_admin->execute();
$admin = $get_admin->fetch();

if(isset($_SESSION['admin_email'])){ 

?>
<!DOCTYPE HTML>
<html>
<head>
<title>Venika | Leadership Team</title>
<link rel="shortcut icon" href="../images/short_icon1.png"/>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Venika's mission is to provide clients and market place leaders with solutions and services that help them solve their business and talent problems. Our deep expertise is in the space of Business & Talent Consulting, Executive Search, Talent Management, Regulatory & Statutory Consulting and Talent process outsourcing. Our offerings include Recruitment Process Outsourcing (RPO), Temporary and Flexi Staffing, Contractor Placement and 
Payroll Management." />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>

<!-- Bootstrap Core CSS -->
<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
<!-- Data Tables -->
<link href="data_tables/css/jquery.dataTables.css" rel='stylesheet' type='text/css' />
<!-- End Data Tables -->
<!-- Custom CSS -->
<link href="css/style.css" rel='stylesheet' type='text/css' />

<!-- font-awesome icons CSS -->
<link href="css/font-awesome.css" rel="stylesheet"> 
<!-- //font-awesome icons CSS -->

 <!-- side nav css file -->
 <link href='css/SidebarNav.min.css' media='all' rel='stylesheet' type='text/css'/>
 <!-- side nav css file -->
 
 <!-- js-->
<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/modernizr.custom.js"></script>

<!--webfonts-->
<link href="//fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i&amp;subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet">
<!--//webfonts--> 

<!-- Metis Menu -->
<script src="js/metisMenu.min.js"></script>
<script src="js/custom.js"></script>
<link href="css/custom.css" rel="stylesheet">
<!--//Metis Menu -->
<style>
.para{
    text-align:justify;
    text-indent: 50px;
}
.dt-buttons{
		margin-bottom: 20px;
    }
.left{
		float: left;margin: 3px 2px 0px 4px;
	}
</style>
</head> 
<body class="cbp-spmenu-push">
	<div class="main-content">
	<div class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="cbp-spmenu-s1">
		<!--left-fixed -navigation-->
		<?php include 'menu.php'; ?>
	</div>
		<!--left-fixed -navigation-->
		
		<!-- Start Notification Count -->
		<?php include 'header.php'; ?>
		<!-- End Notification Count -->
		<!-- main content start-->
		<div id="page-wrapper">
			<div class="main-page">
            <div class="tables">
					<h2 class="title1"><a href="management.php">Management</a> // Leadership Team <!-- <a href="add_leadership_team.php" class="btn btn-success">Add New</a> --></h2>
					<div class="table-responsive bs-example widget-shadow">
						<!-- <h4>Telecaller's :</h4> -->
						<table class="table table-striped">
							<thead>
								<tr>
									<th class="no-export">S.No</th>
									<th class="no-export" width="15%">Image</th>
									<th width="15%">Name</th>
									<th width="20%">Designation</th>
									<th class="" width="30%">Description</th>
									<th width="10%">Created</th>
									<th class="no-export" width="20%">Action</th>
								</tr>
							</thead>
							<tbody>
								<?php
									$stmt = $DB_con->prepare("select * from leadership where data_delete = 0 ORDER BY leader_id DESC");

									$stmt->execute();
									if($stmt->rowCount() > 0)
									{
									while($row=$stmt->fetch(PDO::FETCH_ASSOC))
									{
									extract($row);
								?>
								<tr>
									<th></th>
									<th scope="row"><img src="db/leader_image/<?php echo $leader_image;?>" style="height:200px;width:200px;"></th>
									<td><?php echo $leader_name; ?></td>
									<td><?php echo $leader_des; ?></td>
									<td class="para"><?php echo $leader_desc; ?></td>
                                    <td>
                                        <?php 
                                            $splitTimeStamp = explode(" ",$created_on);
                                            $created_date = $splitTimeStamp[0];
                                            $created_time = $splitTimeStamp[1];
                                            echo date('d/m/Y',strtotime($created_date))." - ".date("g:i a", strtotime($created_time)); 
                                        ?>
                                    </td>
									<td>
										<label><a href="view_leadership_team.php?leadership_id=<?php echo $leader_id; ?>" data-toggle="tooltip" title="View Leadership ;P"><i class="fa fa-folder-open-o" aria-hidden="true"></i></a></label> |
                                        <label><a href="edit_leadership_team.php?leadership_id=<?php echo $leader_id; ?>" data-toggle="tooltip" title="Edit Leadership :)"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></label> | 
										<label><span data-toggle="tooltip" data-placement="top" title="Delete Leadership :(" class='delete' id='del_<?php echo $id=$leader_id; ?>'><i style="cursor: pointer;" class="fa fa-trash-o" aria-hidden="true"></i></span></label>
									</td>
								</tr>
							<?php
									}
								}
								else
								{
									echo "";
								}

								?>
							</tbody>
							
						</table>
							<!-- Pagination --> 
					</div>
				</div>
			</div>
		</div>
		<!--footer-->
		<?php include 'footer.php'; ?>
        <!--//footer-->
	</div>
	<?php   
}else{
    ?>


<?php
 echo "<script>window.location.href='index.php'</script>";
}
?>
<!-- delete lead -->
<script type="text/javascript">
$(document).ready(function(){
	// Delete 
 	$('.delete').click(function(){
		var response = confirm("Sure To Delete ?");
		if ( response == true )
		{
			var el = this;
			var id = this.id;
			var splitid = id.split("_");
			// Delete id
			var deleteid = splitid[1];
	
			// AJAX Request
			$.ajax({
				url: 'db/delete_leadership_team.php',
				type: 'POST',
				data: { id:deleteid },
				success: function(response){
					if(response == 'ok'){
						// Removing row from HTML Table
						$(el).closest('tr').css('background','Red');
						$(el).closest('tr').fadeOut(800, function(){ 
							$(this).remove();
						});	
					}else{
						$('.statusMsg').html('<span style="font-size:18px;color:#EA4335">Some problem occurred, please try again.</span>');
					}
				}
		});
			}else{
				alert("A not fine choice!")
			}
	});

});
</script>
<!-- // delete lead -->
<!-- tooltip -->
	<script>
		$(document).ready(function(){
		    $('[data-toggle="popover"]').popover();  
		});
	</script>
	<script>
		$(document).ready(function(){
		    $('[data-toggle="tooltip"]').tooltip();   
		});
	</script>
	<!-- //tooltip -->
	<!-- side nav js -->
	<script src='js/SidebarNav.min.js' type='text/javascript'></script>
	<script>
      $('.sidebar-menu').SidebarNav()
    </script>
	<!-- //side nav js -->
	
	<!-- Classie --><!-- for toggle left push menu script -->
		<script src="js/classie.js"></script>
		<script>
			var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
				showLeftPush = document.getElementById( 'showLeftPush' ),
				body = document.body;
				
			showLeftPush.onclick = function() {
				classie.toggle( this, 'active' );
				classie.toggle( body, 'cbp-spmenu-push-toright' );
				classie.toggle( menuLeft, 'cbp-spmenu-open' );
				disableOther( 'showLeftPush' );
			};
			
			function disableOther( button ) {
				if( button !== 'showLeftPush' ) {
					classie.toggle( showLeftPush, 'disabled' );
				}
			}
		</script>
	<!-- //Classie --><!-- //for toggle left push menu script -->
	
	<!--scrolling js-->
	<script src="js/jquery.nicescroll.js"></script>
	<script src="js/scripts.js"></script>
	<!--//scrolling js-->
	
	<!-- Bootstrap Core JavaScript -->
   <script src="js/bootstrap.js"> </script>
   <!-- Data Tables -->
	<script src="data_tables/js/jquery.dataTables.js"></script>
	<script src="data_tables/js/dataTables.buttons.min.js"></script>
	<script src="data_tables/js/jszip.min.js"></script>
	<script src="data_tables/js/pdfmake.min.js"></script>
	<script src="data_tables/js/vfs_fonts.js"></script>
	<script src="data_tables/js/buttons.html5.min.js"></script>
	<script src="data_tables/js/buttons.print.min.js"></script>

	
	<script type="text/javascript">
		$(document).ready(function(){
			var table=$(".table").DataTable({
				dom: 'Blfrtip',
				lengthMenu:[
					[10,25,50,-1],
					["10","25","50","all"]
				],
				
       		buttons: [
       		{
       			extend: 'excel',
       			text: 'Excel',
       			className: 'btn btn-success',
       			exportOptions:{
       				columns: ':not(.no-export)'
       			},
       			title: "Leadership Team"
       		},
       		{
       			extend: 'pdf',
       			text: 'PDF',
       			className: 'btn btn-danger',
       			exportOptions:{
       				columns: ':not(.no-export)'
       			},
       			title: "Leadership Team"
       		},
       		{
       			extend: 'print',
       			text: 'Print',
       			className: 'btn btn-warning',
       			exportOptions:{
       				columns: ':not(.no-export)'
       			},
       			title: "Leadership Team"
       		}
       		]
			});
			table.on('order.dt search.dt', function(){
				table.column(0,{search: 'applied',order: 'applied'}).nodes().each(function(cell, index){
					cell.innerHTML=index+1;
				});
			}).draw();
		});
	</script>
	<script>
		$(document).ready(function(){
		    $('[data-toggle="tooltip"]').tooltip();   
		});
	</script>
	<!-- End -->
</body>
</html>