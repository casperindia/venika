
<?php
require_once 'db/dbconfig.php';
$admin_email = $_SESSION['admin_email'];

$get_admin =$DB_con->prepare(" select * from account_user WHERE admin_email = '$admin_email'");
$get_admin->execute();
$admin = $get_admin->fetch();

if(isset($_SESSION['admin_email'])){ 

?>
<!DOCTYPE HTML>
<html>
<head>
<title>Venika | Home</title>
<link rel="shortcut icon" href="../images/short_icon1.png"/>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Venika's mission is to provide clients and market place leaders with solutions and services that help them solve their business and talent problems. Our deep expertise is in the space of Business & Talent Consulting, Executive Search, Talent Management, Regulatory & Statutory Consulting and Talent process outsourcing. Our offerings include Recruitment Process Outsourcing (RPO), Temporary and Flexi Staffing, Contractor Placement and 
Payroll Management." />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>

<!-- Bootstrap Core CSS -->
<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />

<!-- Custom CSS -->
<link href="css/style.css" rel='stylesheet' type='text/css' />

<!-- font-awesome icons CSS -->
<link href="css/font-awesome.css" rel="stylesheet"> 
<!-- //font-awesome icons CSS -->

 <!-- side nav css file -->
 <link href='css/SidebarNav.min.css' media='all' rel='stylesheet' type='text/css'/>
 <!-- side nav css file -->
 
 <!-- js-->
<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/modernizr.custom.js"></script>

<!--webfonts-->
<link href="//fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i&amp;subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet">
<!--//webfonts--> 

<!-- Metis Menu -->
<script src="js/metisMenu.min.js"></script>
<script src="js/custom.js"></script>
<link href="css/custom.css" rel="stylesheet">
<!--//Metis Menu -->
<style type="text/css">
	.content-top-1:hover {
   box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
}
</style>
</head> 
<body class="cbp-spmenu-push">
	<div class="main-content">
	<div class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="cbp-spmenu-s1">
		<!--left-fixed -navigation-->
		<?php include 'menu.php'; ?>
	</div>
		<!--left-fixed -navigation-->
		
		<!-- header-starts -->
		<?php include 'header.php'; ?>
		<!-- //header-ends -->
		<!-- main content start-->
		<div id="page-wrapper">
			<div class="main-page">
				<!-- top -->
				<!-- <div class="col_3">
		        	<div class="col-md-3 widget widget1">
		        		<div class="r3_counter_box">
		                    <i class="pull-left fa fa-dollar icon-rounded"></i>
		                    <div class="stats">
		                      <h5 class="text-center"><strong>Home</strong></h5>
		                      <span>Total Revenue</span>
		                    </div>
		                </div>
		        	</div>
		        	<div class="col-md-3 widget widget1">
		        		<div class="r3_counter_box">
		                    <i class="pull-left fa fa-laptop user1 icon-rounded"></i>
		                    <div class="stats">
		                    </div>
		                </div>
		        	</div>
		        	<div class="col-md-3 widget widget1">
		        		<div class="r3_counter_box">
		                    <i class="pull-left fa fa-money user2 icon-rounded"></i>
		                    <div class="stats">
		                      <h5><strong>$1012</strong></h5>
		                      <span>Expenses</span>
		                    </div>
		                </div>
		        	</div>
		        	<div class="col-md-3 widget widget1">
		        		<div class="r3_counter_box">
		                    <i class="pull-left fa fa-pie-chart dollar1 icon-rounded"></i>
		                    <div class="stats">
		                      <h5><strong>$450</strong></h5>
		                      <span>Expenditure</span>
		                    </div>
		                </div>
		        	</div>
		        	<div class="col-md-3 widget">
		        		<div class="r3_counter_box">
		                    <i class="pull-left fa fa-users dollar2 icon-rounded"></i>
		                    <div class="stats">
		                      <h5><strong>1450</strong></h5>
		                      <span>Total Users</span>
		                    </div>
		                </div>
		        	</div>
        			<div class="clearfix"> </div>
				</div> -->
				<!-- //End -->
				<div class="tables">
					<h2 class="title1">Home</h2>
					<div class="col-md-12">
						<div class="col-md-6">
							<a href="show_banner.php">
							<div class="content-top-1">
								<div class="col-md-6 top-content">
									<label>BANNER VIDEO</label>
                                    <h5>Banner Control's</h5>
								</div>
								<div class="col-md-6 top-content1">	   
									<img src="images/home_slider.png" class="img-responsive icon_panel">
								</div>
							<div class="clearfix"> </div>
							</div>
						</a>
						</div>
						<!-- <div class="col-md-6">
							<a href="show_content_management.php">
							<div class="content-top-1">
								<div class="col-md-6 top-content">
                                    <label>BRIEFLY US</label>
                                    <h5>Content Management</h5>
								</div>
								<div class="col-md-6 top-content1">	   
								<img src="images/icon_2.png" class="img-responsive icon_panel">
								</div>
							<div class="clearfix"> </div>
							</div>
						</a>
						</div>
						
					</div>
                    <div class="col-md-12">
						<div class="col-md-6">
							<a href="show_home_offering.php">
							<div class="content-top-1">
								<div class="col-md-6 top-content">
									<label>OFFERINGS DETAILS</label>
                                    <h5>Offering Details</h5>
								</div>
								<div class="col-md-6 top-content1">	   
									<img src="images/icon_7.png" class="img-responsive icon_panel">
								</div>
							<div class="clearfix"> </div>
							</div>
							</a>
						</div>
						<div class="col-md-6">
							<a href="blog_insights.php">
							<div class="content-top-1">
								<div class="col-md-6 top-content">
									<label>INSIGHTS</label>
                                    <h5>Insights Control's</h5>
								</div>
								<div class="col-md-6 top-content1">	   
									<img src="images/icon_5.png" class="img-responsive icon_panel">
								</div>
							<div class="clearfix"> </div>
							</div>
						</a>
						</div> -->
					</div>
				</div>
			</div>
		</div>
		<!--footer-->
		<?php include 'footer.php'; ?>
        <!--//footer-->
	</div>
	<?php   
}else{
    ?>


<?php
 echo "<script>window.location.href='index.php'</script>";
}
?>
	<!-- side nav js -->
	<script src='js/SidebarNav.min.js' type='text/javascript'></script>
	<script>
      $('.sidebar-menu').SidebarNav()
    </script>
	<!-- //side nav js -->
	
	<!-- Classie --><!-- for toggle left push menu script -->
		<script src="js/classie.js"></script>
		<script>
			var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
				showLeftPush = document.getElementById( 'showLeftPush' ),
				body = document.body;
				
			showLeftPush.onclick = function() {
				classie.toggle( this, 'active' );
				classie.toggle( body, 'cbp-spmenu-push-toright' );
				classie.toggle( menuLeft, 'cbp-spmenu-open' );
				disableOther( 'showLeftPush' );
			};
			
			function disableOther( button ) {
				if( button !== 'showLeftPush' ) {
					classie.toggle( showLeftPush, 'disabled' );
				}
			}
		</script>
	<!-- //Classie --><!-- //for toggle left push menu script -->
	
	<!--scrolling js-->
	<script src="js/jquery.nicescroll.js"></script>
	<script src="js/scripts.js"></script>
	<!--//scrolling js-->
	
	<!-- Bootstrap Core JavaScript -->
	<script src="js/bootstrap.js"> </script>
	
</body>
</html>