<!-- 
Developer : Rehna PV
Company Name : CasperIndia
-->
<?php
require_once 'db/dbconfig.php';
$admin_email = $_SESSION['admin_email'];

$get_admin =$DB_con->prepare(" select * from account_user WHERE admin_email = '$admin_email'");
$get_admin->execute();
$admin = $get_admin->fetch();

if(isset($_SESSION['admin_email'])){ 

$get_video =$DB_con->prepare('select * from add_video where add_video_id =1');
$get_video->execute();
$video = $get_video->fetch();
$add_video_title = $video['add_video_title'];
$add_video_desc = $video['add_video_desc'];
$add_video = $video['add_video'];
$add_video_id = $video['add_video_id'];
?>
<!DOCTYPE HTML>
<html>
<head>
<title>Venika | Add Video</title>
<link rel="shortcut icon" href="../images/short_icon1.png"/>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Venika's mission is to provide clients and market place leaders with solutions and services that help them solve their business and talent problems. Our deep expertise is in the space of Business & Talent Consulting, Executive Search, Talent Management, Regulatory & Statutory Consulting and Talent process outsourcing. Our offerings include Recruitment Process Outsourcing (RPO), Temporary and Flexi Staffing, Contractor Placement and 
Payroll Management." />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>

<!-- Bootstrap Core CSS -->
<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />

<!-- Custom CSS -->
<link href="css/style.css" rel='stylesheet' type='text/css' />

<!-- font-awesome icons CSS -->
<link href="css/font-awesome.css" rel="stylesheet"> 
<!-- //font-awesome icons CSS -->

 <!-- side nav css file -->
 <link href='css/SidebarNav.min.css' media='all' rel='stylesheet' type='text/css'/>
 <!-- side nav css file -->
 
 <!-- js-->
<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/modernizr.custom.js"></script>

<!--webfonts-->
<link href="//fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i&amp;subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet">
<!--//webfonts--> 

<!-- Metis Menu -->
<script src="js/metisMenu.min.js"></script>
<script src="js/custom.js"></script>
<link href="css/custom.css" rel="stylesheet">
<!--//Metis Menu -->
<style>
.para{
	text-align:justify;
}
</style>
</head> 
<body class="cbp-spmenu-push">
	<div class="main-content">
	<div class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="cbp-spmenu-s1">
		<!--left-fixed -navigation-->
		<?php include 'menu.php'; ?>
	</div>
		<!--left-fixed -navigation-->
		
		<!-- Start Notification Count -->
		<?php include 'header.php'; ?>
		<!-- End Notification Count -->
		<!-- main content start-->
		<div id="page-wrapper">
			<div class="main-page">
				<div class="forms">
					<div class="row">
						<h3 class="title1"><a href="home_page.php"> Home</a> // Video </h3>
						<div class="form-three widget-shadow">
							<form class="form-horizontal" enctype="multipart/form-data" id="insert_form" method="post">
								<h4>Video</h4>
								<br>
								<div class="form-group">
									<label for="Emp_code" class="col-sm-2 control-label">Video Title</label>
									<div class="col-sm-8">
										<label class="control-label"><b><?php echo $add_video_title; ?></b></label>
									</div>
								</div>
								<div class="form-group">
									<label for="Emp_code" class="col-sm-2 control-label">Description</label>
									<div class="col-sm-10">
										<label class="para"><b><?php echo $add_video_desc; ?></b></label>
									</div>
								</div>
				                <div class="form-group">
									<label for="Emp_code" class="col-sm-2 control-label">Video Link</label>
									<div class="col-sm-8">
										<label class="control-label"><b><?php echo $add_video; ?></b></label>
									</div>
								</div>
								<div class="form-group">
									<label for="Emp_code" class="col-sm-2 control-label"></label>
									<div class="col-sm-10">
									<a href="edit_video.php?add_video_id=<?php echo $add_video_id; ?>" class="btn btn-warning" >EDIT</a>
									<a href="home_page.php" class="btn btn-danger" >Go Back</a>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--footer-->
		<?php include 'footer.php'; ?>
        <!--//footer-->
	</div>
	<?php   
}else{
    ?>


<?php
 echo "<script>window.location.href='index.php'</script>";
}
?>
<!-- tooltip -->
	<script>
		$(document).ready(function(){
		    $('[data-toggle="popover"]').popover();  
		});
	</script>
	<script>
		$(document).ready(function(){
		    $('[data-toggle="tooltip"]').tooltip();   
		});
	</script>
	<!-- //tooltip -->
	<!-- side nav js -->
	<script src='js/SidebarNav.min.js' type='text/javascript'></script>
	<script>
      $('.sidebar-menu').SidebarNav()
    </script>
	<!-- //side nav js -->
	
	<!-- Classie --><!-- for toggle left push menu script -->
		<script src="js/classie.js"></script>
		<script>
			var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
				showLeftPush = document.getElementById( 'showLeftPush' ),
				body = document.body;
				
			showLeftPush.onclick = function() {
				classie.toggle( this, 'active' );
				classie.toggle( body, 'cbp-spmenu-push-toright' );
				classie.toggle( menuLeft, 'cbp-spmenu-open' );
				disableOther( 'showLeftPush' );
			};
			
			function disableOther( button ) {
				if( button !== 'showLeftPush' ) {
					classie.toggle( showLeftPush, 'disabled' );
				}
			}
		</script>
	<!-- //Classie --><!-- //for toggle left push menu script -->
	
	<!--scrolling js-->
	<script src="js/jquery.nicescroll.js"></script>
	<script src="js/scripts.js"></script>
	<!--//scrolling js-->
	
	<!-- Bootstrap Core JavaScript -->
   <script src="js/bootstrap.js"> </script>
   
</body>
</html>