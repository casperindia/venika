<?php
require_once 'dbconfig.php';
error_reporting( ~E_NOTICE ); // avoid notice
if(!empty($_POST['under_imp_id']) || !empty($_POST['under_imp_name'])){
    
  $under_imp_id = $_POST['under_imp_id']; 
   

    $under_imp_name = $_POST['under_imp_name'];
    $under_imp_capacity = $_POST['under_imp_capacity'];
    $under_imp_location = $_POST['under_imp_location'];
    $under_imp_state = $_POST['under_imp_state'];
    
  /*Image*/
        $stmt_edit = $DB_con->prepare('SELECT under_imp_img FROM  under_implementation WHERE under_imp_id =:uid');
        $stmt_edit->execute(array(':uid'=>$under_imp_id));
        $edit_row = $stmt_edit->fetch(PDO::FETCH_ASSOC);
        extract($edit_row);

        $imgFile = $_FILES['under_imp_img']['name'];
        $tmp_dir = $_FILES['under_imp_img']['tmp_name'];
        $imgSize = $_FILES['under_imp_img']['size'];
                    
        if($imgFile)
        {
            $upload_dir = 'imp_images/'; // upload directory 
            $imgExt = strtolower(pathinfo($imgFile,PATHINFO_EXTENSION)); // get image extension
            $valid_extensions = array('jpeg', 'jpg', 'png', 'gif'); // valid extensions
            $userpic = $emp_code.rand(1000,1000000).".".$imgExt;
            if(in_array($imgExt, $valid_extensions))
            {           
                if($imgSize < 5000000)
                {
                    unlink($upload_dir.$edit_row['under_imp_img']);
                    move_uploaded_file($tmp_dir,$upload_dir.$userpic);
                }
                else
                {
                    $errMSG = "Sorry, your file is too large it should be less then 5MB";
                }
            }
            else
            {
                $errMSG = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";        
            }   
        }
        else
        {
            // if no image selected the old image remain as it is.
            $userpic = $edit_row['under_imp_img']; // old image from database

        }   
        /*Image ENd*/

    //insert form data in the database


    $under_implementation = $DB_con->prepare("UPDATE  under_implementation 
                                SET under_imp_name ='".$under_imp_name."',
                                    under_imp_capacity ='".$under_imp_capacity."',
                                    under_imp_location ='".$under_imp_location."',
                                    under_imp_state ='".$under_imp_state."',
                                    under_imp_img ='".$userpic."',
                                    updated_on = now()
                                    WHERE under_imp_id='".$under_imp_id."'");

    $under_implementation->execute();


    
    echo $under_implementation?'ok':'err';
    
}

?>