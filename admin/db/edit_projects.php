<?php
require_once 'dbconfig.php';
error_reporting( ~E_NOTICE ); // avoid notice
if(!empty($_POST['home_project_id']) || !empty($_POST['home_project_section'])){
    
  $home_project_id = $_POST['home_project_id']; 
   

    $home_project_section_edit = $_POST['home_project_section'];
    $home_project_name_edit = $_POST['home_project_name'];
    $home_project_place_edit = $_POST['home_project_place'];
    
  /*Image*/
        $stmt_edit = $DB_con->prepare('SELECT * FROM home_projects WHERE home_project_id =:uid');
        $stmt_edit->execute(array(':uid'=>$home_project_id));
        $edit_row = $stmt_edit->fetch(PDO::FETCH_ASSOC);
        extract($edit_row);

        $imgFile = $_FILES['home_project_image']['name'];
        $tmp_dir = $_FILES['home_project_image']['tmp_name'];
        $imgSize = $_FILES['home_project_image']['size'];
                    
        if($imgFile)
        {
            $upload_dir = 'project_images/'; // upload directory 
            $imgExt = strtolower(pathinfo($imgFile,PATHINFO_EXTENSION)); // get image extension
            $valid_extensions = array('jpeg', 'jpg', 'png', 'gif'); // valid extensions
            $userpic = $emp_code.rand(1000,1000000).".".$imgExt;
            if(in_array($imgExt, $valid_extensions))
            {           
                if($imgSize < 5000000)
                {
                    unlink($upload_dir.$edit_row['home_project_image']);
                    move_uploaded_file($tmp_dir,$upload_dir.$userpic);
                }
                else
                {
                    $errMSG = "Sorry, your file is too large it should be less then 5MB";
                }
            }
            else
            {
                $errMSG = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";        
            }   
        }
        else
        {
            // if no image selected the old image remain as it is.
            $userpic = $edit_row['home_project_image']; // old image from database

        }   
        /*Image ENd*/

    //insert form data in the database


    $home_projects = $DB_con->prepare("UPDATE home_projects 
                                SET home_project_section ='".$home_project_section_edit."',
                                    home_project_name ='".$home_project_name_edit."',
                                    home_project_place ='".$home_project_place_edit."',
                                    home_project_image ='".$userpic."',
                                    updated_on = now()
                                    WHERE home_project_id='".$home_project_id."'");

    $home_projects->execute();


    
    echo $home_projects?'ok':'err';
    
}

?>