<?php
require_once 'dbconfig.php';
error_reporting( ~E_NOTICE ); // avoid notice
if(!empty($_POST['social_img_id']) || !empty($_POST['social_img_id'])){
    
    $social_img_id = $_POST['social_img_id']; 


        /*Image*/

        $stmt_edit = $DB_con->prepare('SELECT * FROM social_image WHERE social_img_id =:uid');
        $stmt_edit->execute(array(':uid'=>$social_img_id));
        $edit_row = $stmt_edit->fetch(PDO::FETCH_ASSOC);
        extract($edit_row);

        $imgFile = $_FILES['social_img_image']['name'];
        $tmp_dir = $_FILES['social_img_image']['tmp_name'];
        $imgSize = $_FILES['social_img_image']['size'];
                    
        if($imgFile)
        {
            $upload_dir = 'social_slide_images/'; // upload directory 
            $imgExt = strtolower(pathinfo($imgFile,PATHINFO_EXTENSION)); // get image extension
            $valid_extensions = array('jpeg', 'jpg', 'png', 'gif'); // valid extensions
            $userpic = $emp_code.rand(1000,1000000).".".$imgExt;
            if(in_array($imgExt, $valid_extensions))
            {           
                if($imgSize < 5000000)
                {
                    unlink($upload_dir.$edit_row['social_img_image']);
                    move_uploaded_file($tmp_dir,$upload_dir.$userpic);
                }
                else
                {
                    $errMSG = "Sorry, your file is too large it should be less then 5MB";
                }
            }
            else
            {
                $errMSG = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";        
            }   
        }
        else
        {
            // if no image selected the old image remain as it is.
            $userpic = $edit_row['social_img_image']; // old image from database

        }   
        /*Image ENd*/

    //insert form data in the database


    $social_image = $DB_con->prepare("UPDATE social_image 
                                SET social_img_image ='".$userpic."',
                                    updated_on = now()
                                    WHERE social_img_id='".$social_img_id."'");

    $social_image->execute();


    
    echo $social_image?'ok':'err';
    
}
?>