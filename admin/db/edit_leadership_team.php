<?php
require_once 'dbconfig.php';
error_reporting( ~E_NOTICE ); // avoid notice
if(!empty($_POST['leader_name']) || !empty($_POST['leader_id'])){
    
    $leader_name_edit = $_POST['leader_name']; 
    $leader_des_edit = $_POST['leader_des']; 
    $leader_id = $_POST['leader_id'];
    $leader_desc_edit = $_POST['leader_desc'];
    $leader_role_edit = $_POST['leader_role'];
    $f_link_edit = $_POST['f_link'];
    $t_link_edit= $_POST['t_link'];


        $stmt_edit = $DB_con->prepare('SELECT * FROM leadership WHERE leader_id =:uid');
        $stmt_edit->execute(array(':uid'=>$leader_id));
        $edit_row = $stmt_edit->fetch(PDO::FETCH_ASSOC);
        extract($edit_row);

        $imgFile = $_FILES['leader_image']['name'];
        $tmp_dir = $_FILES['leader_image']['tmp_name'];
        $imgSize = $_FILES['leader_image']['size'];
                    
        if($imgFile)
        {
            $upload_dir = 'leader_image/'; // upload directory 
            $imgExt = strtolower(pathinfo($imgFile,PATHINFO_EXTENSION)); // get image extension
            $valid_extensions = array('jpeg', 'jpg', 'png', 'gif'); // valid extensions
            $userpic = $emp_code.rand(1000,1000000).".".$imgExt;
            if(in_array($imgExt, $valid_extensions))
            {           
                if($imgSize < 5000000)
                {
                    unlink($upload_dir.$edit_row['leader_image']);
                    move_uploaded_file($tmp_dir,$upload_dir.$userpic);
                }
                else
                {
                    $errMSG = "Sorry, your file is too large it should be less then 5MB";
                }
            }
            else
            {
                $errMSG = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";        
            }   
        }
        else
        {
            // if no image selected the old image remain as it is.
            $userpic = $edit_row['leader_image']; // old image from database

        }   

        
    //insert form data in the database


    $leadership = $DB_con->prepare("UPDATE leadership 
                                SET leader_name ='".$leader_name_edit."',
                                    leader_des ='".$leader_des_edit."',
                                    leader_desc ='".$leader_desc_edit."',
                                    leader_role ='".$leader_role_edit."',
                                    f_link ='".$f_link_edit."',
                                    t_link ='".$t_link_edit."',
                                    leader_image ='".$userpic."',
                                    updated_on = now()
                                    WHERE leader_id='".$leader_id."'");

    $leadership->execute();


    
    echo $leadership?'ok':'err';

}
?>