<?php
require_once 'dbconfig.php';
error_reporting( ~E_NOTICE ); // avoid notice
if(!empty($_POST['com_id']) || !empty($_POST['com_name'])){
    
  $com_id = $_POST['com_id']; 
   

    $com_name_edit = $_POST['com_name'];
    $com_capacity_edit= $_POST['com_capacity'];
    $com_location_edit= $_POST['com_location'];
    $com_state_edit= $_POST['com_state'];
    
  /*Image*/
        $stmt_edit = $DB_con->prepare('SELECT com_img FROM commissioned WHERE com_id =:uid');
        $stmt_edit->execute(array(':uid'=>$com_id));
        $edit_row = $stmt_edit->fetch(PDO::FETCH_ASSOC);
        extract($edit_row);

        $imgFile = $_FILES['com_img']['name'];
        $tmp_dir = $_FILES['com_img']['tmp_name'];
        $imgSize = $_FILES['com_img']['size'];
                    
        if($imgFile)
        {
            $upload_dir = 'com_image/'; // upload directory 
            $imgExt = strtolower(pathinfo($imgFile,PATHINFO_EXTENSION)); // get image extension
            $valid_extensions = array('jpeg', 'jpg', 'png', 'gif'); // valid extensions
            $userpic = $emp_code.rand(1000,1000000).".".$imgExt;
            if(in_array($imgExt, $valid_extensions))
            {           
                if($imgSize < 5000000)
                {
                    unlink($upload_dir.$edit_row['com_img']);
                    move_uploaded_file($tmp_dir,$upload_dir.$userpic);
                }
                else
                {
                    $errMSG = "Sorry, your file is too large it should be less then 5MB";
                }
            }
            else
            {
                $errMSG = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";        
            }   
        }
        else
        {
            // if no image selected the old image remain as it is.
            $userpic = $edit_row['com_img']; // old image from database

        }   
        /*Image ENd*/

    //insert form data in the database


    $commissioned = $DB_con->prepare("UPDATE commissioned 
                                SET com_name ='".$com_name_edit."',
                                    com_capacity ='".$com_capacity_edit."',
                                    com_location ='".$com_location_edit."',
                                    com_state ='".$com_state_edit."',
                                    com_img ='".$userpic."',
                                    updated_on = now()
                                    WHERE com_id='".$com_id."'");

    $commissioned->execute();


    
    echo $commissioned?'ok':'err';
    
}

?>