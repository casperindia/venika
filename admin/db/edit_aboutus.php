<?php
require_once 'dbconfig.php';
error_reporting( ~E_NOTICE ); // avoid notice
if(!empty($_POST['about_id']) || !empty($_POST['about_title_one'])){
    
  $about_id = $_POST['about_id']; 
   

    $about_title_one = $_POST['about_title_one'];
    $about_title_two = $_POST['about_title_two'];
    $about_desc = $_POST['about_desc'];
    
  /*Image*/
        $stmt_edit = $DB_con->prepare('SELECT about_image FROM about_us WHERE about_id =:uid');
        $stmt_edit->execute(array(':uid'=>$about_id));
        $edit_row = $stmt_edit->fetch(PDO::FETCH_ASSOC);
        extract($edit_row);

        $imgFile = $_FILES['about_image']['name'];
        $tmp_dir = $_FILES['about_image']['tmp_name'];
        $imgSize = $_FILES['about_image']['size'];
                    
        if($imgFile)
        {
            $upload_dir = 'about_image/'; // upload directory 
            $imgExt = strtolower(pathinfo($imgFile,PATHINFO_EXTENSION)); // get image extension
            $valid_extensions = array('jpeg', 'jpg', 'png', 'gif'); // valid extensions
            $userpic = $emp_code.rand(1000,1000000).".".$imgExt;
            if(in_array($imgExt, $valid_extensions))
            {           
                if($imgSize < 5000000)
                {
                    unlink($upload_dir.$edit_row['about_image']);
                    move_uploaded_file($tmp_dir,$upload_dir.$userpic);
                }
                else
                {
                    $errMSG = "Sorry, your file is too large it should be less then 5MB";
                }
            }
            else
            {
                $errMSG = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";        
            }   
        }
        else
        {
            // if no image selected the old image remain as it is.
            $userpic = $edit_row['about_image']; // old image from database

        }   
        /*Image ENd*/

    //insert form data in the database


    $about_us = $DB_con->prepare("UPDATE about_us 
                                SET about_title_one ='".$about_title_one."',
                                    about_title_two ='".$about_title_two."',
                                    about_desc ='".$about_desc."',
                                    about_image ='".$userpic."',
                                    updated_on = now()
                                    WHERE about_id='".$about_id."'");

    $about_us->execute();


    
    echo $about_us?'ok':'err';
    
}

?>