<?php
require_once 'dbconfig.php';
error_reporting( ~E_NOTICE ); // avoid notice
if(!empty($_POST['res_id']) || !empty($_POST['res_name'])){
    
    $res_id = $_POST['res_id']; 
    $res_name = $_POST['res_name'];
     $res_desc = $_POST['res_desc'];

    
  /*Image*/
        $stmt_edit = $DB_con->prepare('SELECT res_image FROM social_responsibility WHERE res_id =:uid');
        $stmt_edit->execute(array(':uid'=>$res_id));
        $edit_row = $stmt_edit->fetch(PDO::FETCH_ASSOC);
        extract($edit_row);

        $imgFile = $_FILES['res_image']['name'];
        $tmp_dir = $_FILES['res_image']['tmp_name'];
        $imgSize = $_FILES['res_image']['size'];
                    
        if($imgFile)
        {
            $upload_dir = 'social_images/'; // upload directory 
            $imgExt = strtolower(pathinfo($imgFile,PATHINFO_EXTENSION)); // get image extension
            $valid_extensions = array('jpeg', 'jpg', 'png', 'gif'); // valid extensions
            $userpic = $emp_code.rand(1000,1000000).".".$imgExt;
            if(in_array($imgExt, $valid_extensions))
            {           
                if($imgSize < 5000000)
                {
                    unlink($upload_dir.$edit_row['res_image']);
                    move_uploaded_file($tmp_dir,$upload_dir.$userpic);
                }
                else
                {
                    $errMSG = "Sorry, your file is too large it should be less then 5MB";
                }
            }
            else
            {
                $errMSG = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";        
            }   
        }
        else
        {
            // if no image selected the old image remain as it is.
            $userpic = $edit_row['res_image']; // old image from database

        }   
        /*Image ENd*/

    //insert form data in the database


    $social_responsibility = $DB_con->prepare('UPDATE social_responsibility 
                                SET res_name ="'.$res_name.'",
                                    res_desc ="'.$res_desc.'",
                                    res_image ="'.$userpic.'",
                                    updated_on = now()
                                    WHERE res_id="'.$res_id.'"');

    $social_responsibility->execute();


    
    echo $social_responsibility?'ok':'err';
    
}

?>