<?php
require_once 'dbconfig.php';
error_reporting( ~E_NOTICE ); // avoid notice
if(!empty($_POST['under_construction_id']) || !empty($_POST['under_imp_name'])){
    
  $under_construction_id = $_POST['under_construction_id']; 
   

    $under_construction_name = $_POST['under_construction_name'];
    $under_construction_capacity = $_POST['under_construction_capacity'];
    $under_construction_location = $_POST['under_construction_location'];
    $under_construction_state = $_POST['under_construction_state'];
    
  /*Image*/
        $stmt_edit = $DB_con->prepare('SELECT under_construction_image FROM  under_construction WHERE under_construction_id =:uid');
        $stmt_edit->execute(array(':uid'=>$under_construction_id));
        $edit_row = $stmt_edit->fetch(PDO::FETCH_ASSOC);
        extract($edit_row);

        $imgFile = $_FILES['under_construction_image']['name'];
        $tmp_dir = $_FILES['under_construction_image']['tmp_name'];
        $imgSize = $_FILES['under_construction_image']['size'];
                    
        if($imgFile)
        {
            $upload_dir = 'con_image/'; // upload directory 
            $imgExt = strtolower(pathinfo($imgFile,PATHINFO_EXTENSION)); // get image extension
            $valid_extensions = array('jpeg', 'jpg', 'png', 'gif'); // valid extensions
            $userpic = $emp_code.rand(1000,1000000).".".$imgExt;
            if(in_array($imgExt, $valid_extensions))
            {           
                if($imgSize < 5000000)
                {
                    unlink($upload_dir.$edit_row['under_construction_image']);
                    move_uploaded_file($tmp_dir,$upload_dir.$userpic);
                }
                else
                {
                    $errMSG = "Sorry, your file is too large it should be less then 5MB";
                }
            }
            else
            {
                $errMSG = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";        
            }   
        }
        else
        {
            // if no image selected the old image remain as it is.
            $userpic = $edit_row['under_construction_image']; // old image from database

        }   
        /*Image ENd*/

    //insert form data in the database


    $under_construction = $DB_con->prepare("UPDATE  under_construction 
                                SET under_construction_name ='".$under_construction_name."',
                                    under_construction_capacity ='".$under_construction_capacity."',
                                    under_construction_location ='".$under_construction_location."',
                                    under_construction_state ='".$under_construction_state."',
                                    under_construction_image ='".$userpic."',
                                    updated_on = now()
                                    WHERE under_construction_id='".$under_construction_id."'");

    $under_construction->execute();


    
    echo $under_construction?'ok':'err';
    
}

?>