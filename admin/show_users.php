<!-- 
Developer : KALAISELVAN
Company Name : CasperIndia
-->
<?php
require_once 'db/dbconfig.php';
$admin_email = $_SESSION['admin_email'];

$get_admin =$DB_con->prepare(" select * from account_user WHERE admin_email = '$admin_email'");
$get_admin->execute();
$admin = $get_admin->fetch();

if(isset($_SESSION['admin_email'])){ 

?>
<!DOCTYPE HTML>
<html>
<head>
<title>Venika | Subscribe Users</title>
<link rel="shortcut icon" href="../images/short_icon1.png"/>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="venika's mission is to provide clients and market place leaders with solutions and services that help them solve their business and talent problems. Our deep expertise is in the space of Business & Talent Consulting, Executive Search, Talent Management, Regulatory & Statutory Consulting and Talent process outsourcing. Our offerings include Recruitment Process Outsourcing (RPO), Temporary and Flexi Staffing, Contractor Placement and 
Payroll Management." />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>

<!-- Bootstrap Core CSS -->
<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
<!-- Data Tables -->
<link href="data_tables/css/jquery.dataTables.css" rel='stylesheet' type='text/css' />
<!-- End Data Tables -->
<!-- Custom CSS -->
<link href="css/style.css" rel='stylesheet' type='text/css' />

<!-- font-awesome icons CSS -->
<link href="css/font-awesome.css" rel="stylesheet"> 
<!-- //font-awesome icons CSS -->

 <!-- side nav css file -->
 <link href='css/SidebarNav.min.css' media='all' rel='stylesheet' type='text/css'/>
 <!-- side nav css file -->
 
 <!-- js-->
<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/modernizr.custom.js"></script>

<!--webfonts-->
<link href="//fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i&amp;subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet">
<!--//webfonts--> 
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<!-- Metis Menu -->
<script src="js/metisMenu.min.js"></script>
<script src="js/custom.js"></script>
<link href="css/custom.css" rel="stylesheet">
<!--//Metis Menu -->
<style>
.para{
    text-align:justify;
    text-indent: 50px;
}
.dt-buttons{
		margin-bottom: 20px;
    }
.left{
		float: left;margin: 3px 2px 0px 4px;
	}
	/* Rounded sliders */
.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

.switch input { 
  opacity: 0;
  width: 0;
  height: 0;
}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
</style>
</head> 
<body class="cbp-spmenu-push">
	<div class="main-content">
	<div class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="cbp-spmenu-s1">
		<!--left-fixed -navigation-->
		<?php include 'menu.php'; ?>
	</div>
		<!--left-fixed -navigation-->
		
		<!-- Start Notification Count -->
		<?php include 'header.php'; ?>
		<!-- End Notification Count -->
		<!-- main content start-->
		<div id="page-wrapper">
			<div class="main-page">
            <div class="tables">
					<!-- <h2 class="title1"><a class="btn btn-success" href="show_blog.php">Blog</a> // User</h2> -->
					<h4>Subscriber's <a href="show_active_users	.php" class="btn btn-success">Active Users</a></h4>
					<div class="table-responsive bs-example widget-shadow">
						
						<table class="table table-striped">
							<thead>
								<tr>
									<th>S.No</th>
									<th>Username</th>
									<th>Email</th>
									<th>Created</th>
									<th class="no-export">Action</th>
								</tr>
							</thead>
							<tbody>
								<?php
									$stmt = $DB_con->prepare("select * from login where status = 0 ORDER BY id DESC");

									$stmt->execute();
									if($stmt->rowCount() > 0)
									{
									while($row=$stmt->fetch(PDO::FETCH_ASSOC))
									{
									extract($row);
								?>
								<tr>
									<th scope="row">
										
									</th>
									<!-- <td><?php 
											$get_name =$DB_con->prepare(" select * from login WHERE id = '$id'");
											$get_name->execute();
											$get_name = $get_name->fetch();
											echo $get_name['name'];
										?>
									</td>  -->
									<td>
										<?php echo $name; ?></td>
									<td><?php echo $email; ?></td>
									<td>
                                        <?php 
                                            $splitTimeStamp = explode(" ",$created_on);
                                            $created_date = $splitTimeStamp[0];
                                            $created_time = $splitTimeStamp[1];
                                            echo date('d/m/Y',strtotime($created_date))." - ".date("g:i a", strtotime($created_time)); 
                                        ?>
                                    </td>
									<td>
										<!-- <label class="switch"><input type="checkbox"><span class="slider round assign_user" id='del_<?php echo $id=$id; ?>'></span></label> -->
                                    <label><span class="assign_user" style="cursor: pointer;" id='del_<?php echo $id=$id; ?>'><i class="fa fa-check" aria-hidden="true" data-toggle="tooltip" title="Active" data-placement="left"></i></span></label> | <label><span class="delete_user" style="cursor: pointer;" id='del_<?php echo $id=$id; ?>'><i class="fa fa-times" aria-hidden="true" data-toggle="tooltip" title="Delete" data-placement="right"></i></span></label>
									</td>
								</tr>
							<?php
									}
								}
								else
								{
									echo "";
								}

								?>
							</tbody>
							
						</table>
							<!-- Pagination --> 
					</div>
				</div>
			</div>
		</div>
		<!--footer-->
		<?php include 'footer.php'; ?>
        <!--//footer-->
	</div>
	<?php   
}else{
    ?>


<?php
 echo "<script>window.location.href='index.php'</script>";
}
?>
<!-- Assign lead -->
<script type="text/javascript">
	$(document).ready(function(){
 // Delete 
 $('.assign_user').click(function(){
	var response = confirm("Do you want to Assign ?  Click OK to proceed otherwise click Cancel.");
	if ( response == true )
	{
		var el = this;
		var id = this.id;
		var splitid = id.split("_");

		// Delete id
		var assign_id = splitid[1];
		 
		// AJAX Request
		$.ajax({
		    url: 'db/user_assign.php',
		    type: 'POST',
		    data: { id:assign_id },
		    success: function(response){
		   	if(response == 'ok'){
				// Removing row from HTML Table
			    $(el).closest('tr').css('background','green');
			    $(el).closest('tr').fadeOut(800, function(){ 
			    $(this).remove();
		    	});	
			}else{
			alert('Error');
			}
		   }
		  });
	}
 });

});
</script>
<!-- // Assign lead -->
<!-- Delete  -->
<script type="text/javascript">
	$(document).ready(function(){
	 $('.delete_user').click(function(){
	var response = confirm("Do you want to Delete ?  Click OK to proceed otherwise click Cancel.");
	if ( response == true )
	{
		var el = this;
		var id = this.id;
		var splitid = id.split("_");

		// Delete id
		var delete_id = splitid[1];
		 
		// AJAX Request
		$.ajax({
		    url: 'db/user_delete.php',
		    type: 'POST',
		    data: { id:delete_id },
		    success: function(response){
		   	if(response == 'ok'){
				// Removing row from HTML Table
			    $(el).closest('tr').css('background','red');
			    $(el).closest('tr').fadeOut(800, function(){ 
			    $(this).remove();
		    	});	
			}else{
			alert('Error');
			}
		   }
		  });
	}
 });

});
</script>
<!-- //Delete -->
<!-- tooltip -->
	<script>
		$(document).ready(function(){
		    $('[data-toggle="popover"]').popover();  
		});
	</script>
	<script>
		$(document).ready(function(){
		    $('[data-toggle="tooltip"]').tooltip();   
		});
	</script>
	<!-- //tooltip -->
	<!-- side nav js -->
	<script src='js/SidebarNav.min.js' type='text/javascript'></script>
	<script>
      $('.sidebar-menu').SidebarNav()
    </script>
	<!-- //side nav js -->
	
	<!-- Classie --><!-- for toggle left push menu script -->
		<script src="js/classie.js"></script>
		<script>
			var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
				showLeftPush = document.getElementById( 'showLeftPush' ),
				body = document.body;
				
			showLeftPush.onclick = function() {
				classie.toggle( this, 'active' );
				classie.toggle( body, 'cbp-spmenu-push-toright' );
				classie.toggle( menuLeft, 'cbp-spmenu-open' );
				disableOther( 'showLeftPush' );
			};
			
			function disableOther( button ) {
				if( button !== 'showLeftPush' ) {
					classie.toggle( showLeftPush, 'disabled' );
				}
			}
		</script>
	<!-- //Classie --><!-- //for toggle left push menu script -->
	
	<!--scrolling js-->
	<script src="js/jquery.nicescroll.js"></script>
	<script src="js/scripts.js"></script>
	<!--//scrolling js-->
	
	<!-- Bootstrap Core JavaScript -->
   <script src="js/bootstrap.js"> </script>
   <!-- Data Tables -->
	<script src="data_tables/js/jquery.dataTables.js"></script>
	<script src="data_tables/js/dataTables.buttons.min.js"></script>
	<script src="data_tables/js/jszip.min.js"></script>
	<script src="data_tables/js/pdfmake.min.js"></script>
	<script src="data_tables/js/vfs_fonts.js"></script>
	<script src="data_tables/js/buttons.html5.min.js"></script>
	<script src="data_tables/js/buttons.print.min.js"></script>

	
	<script type="text/javascript">
		$(document).ready(function(){
			var table=$(".table").DataTable({
				dom: 'Blfrtip',
				lengthMenu:[
					[10,25,50,-1],
					["10","25","50","all"]
				],
				
       		buttons: [
       		{
       			extend: 'excel',
       			text: 'Excel',
       			className: 'btn btn-success',
       			exportOptions:{
       				columns: ':not(.no-export)'
       			},
       			title: "Users"
       		},
       		{
       			extend: 'pdf',
       			text: 'PDF',
       			className: 'btn btn-danger',
       			exportOptions:{
       				columns: ':not(.no-export)'
       			},
       			title: "Users"
       		},
       		{
       			extend: 'print',
       			text: 'Print',
       			className: 'btn btn-warning',
       			exportOptions:{
       				columns: ':not(.no-export)'
       			},
       			title: "Users"
       		}
       		]
			});
			table.on('order.dt search.dt', function(){
				table.column(0,{search: 'applied',order: 'applied'}).nodes().each(function(cell, index){
					cell.innerHTML=index+1;
				});
			}).draw();
		});
	</script>
	<script>
		$(document).ready(function(){
		    $('[data-toggle="tooltip"]').tooltip();   
		});
	</script>
	<!-- End -->
</body>
</html>