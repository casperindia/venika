<?php
include 'dbconfig.php';
$email = $_SESSION['email'];

$get_login =$DB_con->prepare(" select * from login WHERE email = '$email' ");
$get_login->execute();
$login = $get_login->fetch();

$get_res_heading =$DB_con->prepare(" select * from res_heading WHERE res_id = '1'");
$get_res_heading->execute();
$res_heading = $get_res_heading->fetch();

$get_one_responsibility =$DB_con->prepare(" select * from social_responsibility WHERE res_id = '1'");
$get_one_responsibility->execute();
$get_one_r = $get_one_responsibility->fetch();

$get_two_responsibility =$DB_con->prepare(" select * from social_responsibility WHERE res_id = '2'");
$get_two_responsibility->execute();
$get_two_r = $get_two_responsibility->fetch();

$get_three_responsibility =$DB_con->prepare(" select * from social_responsibility WHERE res_id = '3'");
$get_three_responsibility->execute();
$get_three_r = $get_three_responsibility->fetch();

$get_four_responsibility =$DB_con->prepare(" select * from social_responsibility WHERE res_id = '4'");
$get_four_responsibility->execute();
$get_four_r = $get_four_responsibility->fetch();
?>


<!DOCTYPE html>
<html>

<head>
	<title>Venika | Social Responsibility </title>
	<!--/tags -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="Conceit Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
	<script type="application/x-javascript">
		addEventListener("load", function () {
			setTimeout(hideURLbar, 0);
		}, false);

		function hideURLbar() {
			window.scrollTo(0, 1);
		}
	</script>
	<!--//tags -->
	<link rel="shortcut icon" href="images/home/venika-icon.png"/>
	<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
	<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
	<link href="css/custom.css" rel="stylesheet" type="text/css" media="all" />
	<link href="css/team.css" rel="stylesheet" type="text/css" media="all" />
	<link href="css/font-awesome.css" rel="stylesheet">
	<!-- //for bootstrap working -->
	<link href="//fonts.googleapis.com/css?family=Raleway:100,100i,200,300,300i,400,400i,500,500i,600,600i,700,800" rel="stylesheet">
	<link href="//fonts.googleapis.com/css?family=Source+Sans+Pro:300,300i,400,400i,600,600i,700" rel="stylesheet">
	<!-- tab-&-img-modal-popup -->
	<link rel="stylesheet" href="w3css/4/w3.css">
	<!-- //End -->
</head>

<body style="text-align: justify;">
	<!-- header-top -->
	<div class="top_header" id="home">
		<!-- Fixed navbar -->
		<nav class="navbar navbar-default navbar-fixed-top">
			<div class="nav_top_fx_w3ls_agileinfo">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false"
					    aria-controls="navbar">
				        <span class="sr-only">Toggle navigation</span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				    </button>
					<div class="logo-w3layouts-agileits">
						<h1>
							<a class="navbar-brand" href="index.php">
								<!-- <i class="fa fa-clone" aria-hidden="true"></i> Conceit <span class="desc">For your Business</span> -->
								<img src="images/home/venika-logo-head.png" alt="" class="img-responsive">
							</a>
						</h1>
					</div>
				</div>
				<div id="navbar" class="navbar-collapse collapse">
					<div class="nav_right_top">
						<ul class="nav navbar-nav">
							<li><a href="index.php">Home</a></li>
							<li><a href="about.php">About Us</a></li>
							<li><a href="management.php">Management</a></li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Projects <span class="caret"></span></a>
								<ul class="dropdown-menu">
									<li><a href="under-implementation.php">UNDER IMPLEMENTATION</a></li>
									<li><a href="under-construction.php">UNDER CONSTRUCTION</a></li>
									<li><a href="commissioned.php">COMMISSIONED</a></li>
								</ul>
							</li>
							<li class="active"><a href="social-responsibility.php">Social Responsibility</a></li>
							<li><a href="contact.php">Contact</a></li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user"></i>    <?php echo $login['name']; ?> <span class="caret"></span></a>
								<ul class="dropdown-menu">
									<li><a href="logout.php">Logout</a></li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
				<!--/.nav-collapse -->
			</div>
		</nav>
	</div>
	<!-- //End -->

	<!--/banner_info-->
	<div class="banner_inner_con"> </div>
	<div class="services-breadcrumb">
		<div class="inner_breadcrumb">
			<ul class="short">
				<li><a href="index.php">Home</a><span>|</span></li>
				<li>Social Responsibility</li>
			</ul>
		</div>
	</div>
	<!--//banner_info-->
	
	<!--/bottom-->
	<div class="banner_bottom">
		<div class="container">
			
              
               <h3 class="tittle-w3ls"><?php echo $res_heading['res_title_one']; ?></h3>
			<div class="inner_sec_info_wthree_agile">
				<div class="help_full">
					<div class="col-md-6 banner_bottom_left">
						<h4><?php echo $res_heading['res_title_two']; ?></h4>
						<p><?php echo $res_heading['res_desc']; ?></p>
						<div class="ab_button marquee-btn">
							<a class="btn btn-primary btn-lg hvr-underline-from-left" href="about.php" role="button">
							  <marquee behavior="scroll" direction="left" onmouseover="this.stop();" onmouseout="this.start();">
					       		Venika has been successfully operating in the Indian Renewable Energy sector since 2005. The company has successfully developed and commissioned a canal based Mini Hydel Project in Karnataka (0.75 MW).
					       	  </marquee>
							</a>
						</div>
					</div>
			
				<div class="col-md-6 banner_bottom_grid help">
						<div id="about-img-carousel">
							<div id="myCarousel" class="carousel slide" data-ride="carousel">
							    <ol class="carousel-indicators" style="margin-bottom: 0px; z-index: 1;">
							      	<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
							      	<li data-target="#myCarousel" data-slide-to="1"></li>
							      	<li data-target="#myCarousel" data-slide-to="2"></li>
							    </ol>
							    <div class="carousel-inner">
							    	 <?php

								        $stmt_slider_social = $DB_con->prepare('SELECT * FROM social_image');
								        $stmt_slider_social->execute();
								        
								        if($stmt_slider_social->rowCount() > 0)
								        {
								        while($row_slider_social=$stmt_slider_social->fetch(PDO::FETCH_ASSOC))
								        {
								          extract($row_slider_social);
								       ?>
							      	<div class="item <?php if($row_slider_social['social_img_id'] == '1'){ echo 'active';} ?>">
							        	<img src="admin/db/social_slide_images/<?php echo $row_slider_social['social_img_image']; ?>" alt="cad" class="img-responsive">
							      	</div>
							      	<?php
								        }
								        }
								        else
								        {

								        ?>
								              <div class="col-xs-12">
								               <div class="alert alert-warning" style="font-size: 12px !important; background: #fff; border: none; padding: 1em 0 1em 0;">
								                 <span class="glyphicon glyphicon-info-sign"></span> &nbsp; No Data Found ...
								                </div>
								              </div>
								              <?php
								        }
								        
								      ?>
							      	<!-- 
							      	<div class="item">
							        	<img src="images/home/sr-slide2.jpg" alt="quantity surveying" class="img-responsive">
							      	</div>
							      	<div class="item">
							        	<img src="images/home/sr-slide3.jpg" alt="mechanical cad" class="img-responsive">
							      	</div> -->
							    </div>
							    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
							      	<span class="glyphicon glyphicon-chevron-left" style="color: #fff;"></span>
							      	<span class="sr-only">Previous</span>
							    </a>
							    <a class="right carousel-control" href="#myCarousel" data-slide="next">
							      	<span class="glyphicon glyphicon-chevron-right" style="color: #fff;"></span>
							      	<span class="sr-only">Next</span>
							    </a>
							</div>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div> 
		</div>
	</div>
              

			<!-- <h3 class="tittle-w3ls">Let’s Protect Our Environment, Keep It Safe; Tomorrow, We’ll Be Saved! </h3>
			<div class="inner_sec_info_wthree_agile">
				<div class="help_full">
					<div class="col-md-6 banner_bottom_left">
						<h4>Environment Makes From You And Me </h4>
						<p>
							We at VENIKA’s realize that we have a commitment to make the society more Livable. This makes it to have a conviction to play a role in bringing about positive social change in the society. Beyond our core hydropower development activities, we care for the upliftment of the local community, protection of the environment and development of infrastructure in its project areas. As part of its Corporate Social Responsibility.
						</p>
						<div class="ab_button marquee-btn">
							<a class="btn btn-primary btn-lg hvr-underline-from-left" href="about.php" role="button">
							  <marquee behavior="scroll" direction="left" onmouseover="this.stop();" onmouseout="this.start();">
					       		Venika has been successfully operating in the Indian Renewable Energy sector since 2005. The company has successfully developed and commissioned a canal based Mini Hydel Project in Karnataka (0.75 MW).
					       	  </marquee>
							</a>
						</div>
					</div>
					<div class="col-md-6 banner_bottom_grid help">
						<div id="about-img-carousel">
							<div id="myCarousel" class="carousel slide" data-ride="carousel">
							    <ol class="carousel-indicators" style="margin-bottom: 0px; z-index: 1;">
							      	<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
							      	<li data-target="#myCarousel" data-slide-to="1"></li>
							      	<li data-target="#myCarousel" data-slide-to="2"></li>
							    </ol>
							    <div class="carousel-inner">
							      	<div class="item active">
							        	<img src="images/home/sr-slide1.jpg" alt="cad" class="img-responsive">
							      	</div>
							      	<div class="item">
							        	<img src="images/home/sr-slide2.jpg" alt="quantity surveying" class="img-responsive">
							      	</div>
							      	<div class="item">
							        	<img src="images/home/sr-slide3.jpg" alt="mechanical cad" class="img-responsive">
							      	</div>
							    </div>
							    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
							      	<span class="glyphicon glyphicon-chevron-left" style="color: #fff;"></span>
							      	<span class="sr-only">Previous</span>
							    </a>
							    <a class="right carousel-control" href="#myCarousel" data-slide="next">
							      	<span class="glyphicon glyphicon-chevron-right" style="color: #fff;"></span>
							      	<span class="sr-only">Next</span>
							    </a>
							</div>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div> --> 
		</div>
	</div>
	<!--//bottom-->
	
	<!--/blog-->
	<div class="blog_sec" style="padding: 0;">
		<h3 class="tittle-w3ls">Social Responsibility</h3>
		<div class="col-md-6 banner-btm-left">
			<div class="banner-btm-top">
				<div class="banner-btm-inner a1">
					<h6><a href="#"><?php echo $get_one_r['res_name']; ?></a></h6>
					<p class="paragraph"><?php echo $get_one_r['res_desc']; ?></p>
					<div class="clearfix"></div>
				</div>
				<div class="banner-btm-inner a2" style=" background: url(admin/db/social_images/<?php echo $get_one_r['res_image']; ?>) no-repeat 0px 0px/cover;">

				</div>
			</div>
			<div class="banner-btm-bottom">
				<div class="banner-btm-inner a3" style=" background: url(admin/db/social_images/<?php echo $get_two_r['res_image']; ?>) no-repeat 0px 0px/cover;">

				</div>
				<div class="banner-btm-inner a4">
					<h6><a href="#"><?php echo $get_two_r['res_name']; ?></a></h6>
					<p class="paragraph"><?php echo $get_two_r['res_desc']; ?></p>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
		<div class="col-md-6 banner-btm-left">
			<div class="banner-btm-top">
				<div class="banner-btm-inner a1">
					<h6><a href="#"><?php echo $get_three_r['res_name']; ?></a></h6>
					<p class="paragraph"><?php echo $get_three_r['res_desc']; ?></p>
					<div class="clearfix"></div>
				</div>
				<div class="banner-btm-inner a5"  style=" background: url(admin/db/social_images/<?php echo $get_three_r['res_image']; ?>) no-repeat 0px 0px/cover;">

				</div>
			</div>
			<div class="banner-btm-bottom">
				<div class="banner-btm-inner a6"  style=" background: url(admin/db/social_images/<?php echo $get_four_r['res_image']; ?>) no-repeat 0px 0px/cover;">

				</div>
				<div class="banner-btm-inner a4">
					<h6><a href="#"><?php echo $get_four_r['res_name']; ?></a></h6>
					<p class="paragraph"><?php echo $get_four_r['res_desc']; ?></p>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
	<!--//blog-->

	<!--/what-->
	<div class="works" style="background: none; padding-top: 0;">
		<div class="container">
			<!--<h3 class="tittle-w3ls">Social Responsibility</h3>-->
			<div class="inner_sec_info_wthree_agile">
				<div class="ser-first" style="border: 2px solid rgb(118, 218, 255); padding: 5px;">
				    <!-- Owl image Popup Start -->
				    <?php

					        $stmt_slider_pop = $DB_con->prepare('SELECT * FROM res_slider where add_slider_page = 3 and data_delete = 0 ORDER BY res_slider_id asc');
					        $stmt_slider_pop->execute();
					        
					        if($stmt_slider_pop->rowCount() > 0)
					        {
					        while($row_slider_pop=$stmt_slider_pop->fetch(PDO::FETCH_ASSOC))
					        {
					          extract($row_slider_pop);
					          ?>
				    <div id="popup_<?php echo $row_slider_pop['res_slider_id'];?>" class="w3-modal w3-modal-img-pop" onclick="this.style.display='none'">
    				    <span class="w3-button-img-pop w3-hover-red w3-xlarge-img-pop w3-display-topright-img-pop">&times;</span>
    				    <div class="w3-modal-content-img-pop w3-animate-zoom">
    				      	<img src="admin/db/slider_image/<?php echo $row_slider_pop['res_slider_image'];?>" class="img-responsive">
    				    </div>
    				</div>
    				<?php
				        }
				        }
				        else
				        {

				        ?>
			              <div class="col-xs-12">
			               <div class="alert alert-warning" style="font-size: 12px !important; background: #fff; border: none; padding: 1em 0 1em 0;">
			                 <span class="glyphicon glyphicon-info-sign"></span> &nbsp; No Data Found ...
			                </div>
			              </div>
			              <?php
				        }
				        
				      ?>
					<div id="owl-demo">
						<?php

					        $stmt_slider = $DB_con->prepare('SELECT * FROM res_slider where add_slider_page = 3 and data_delete = 0 ORDER BY res_slider_id asc');
					        $stmt_slider->execute();
					        
					        if($stmt_slider->rowCount() > 0)
					        {
					        while($row_slider=$stmt_slider->fetch(PDO::FETCH_ASSOC))
					        {
					          extract($row_slider);
					          ?>
						<div class="item">
						    <img src="admin/db/slider_image/<?php echo $row_slider['res_slider_image'];?>" class="img-responsive w3-hover-opacity" onclick="document.getElementById('popup_<?php echo $row_slider['res_slider_id'];?>').style.display='block'" style="cursor:zoom-in; width: 200px;height: 150px;" alt="Owl Image-1">
						</div>
						<?php
					        }
					        }
					        else
					        {

					        ?>
					              <div class="col-xs-12">
					               <div class="alert alert-warning" style="font-size: 12px !important; background: #fff; border: none; padding: 1em 0 1em 0;">
					                 <span class="glyphicon glyphicon-info-sign"></span> &nbsp; No Data Found ...
					                </div>
					              </div>
					              <?php
					        }
					        
					      ?>
					</div>
					<!-- //End -->
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>
	<!--//what-->
	
	<!-- Footer-start-here -->
	<?php include 'footer.php'; ?>
	<!-- //Footer-end-here -->

	<!-- js -->
	<script src="js/responsiveslides.min.js"></script>
      <script>
         $(function () {
          $("#slider3").responsiveSlides({
            auto: true,
            pager:true,
            nav: false,
            speed: 900,
            namespace: "callbacks",
            before: function () {
              $('.events').append("<li>before event fired.</li>");
            },
            after: function () {
              $('.events').append("<li>after event fired.</li>");
            }
          });
         
         });
      </script>
	<script type="text/javascript" src="js/jquery-2.2.3.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.js"></script>
	<script>
		$('ul.dropdown-menu li').hover(function () {
			$(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
		}, function () {
			$(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
		});
	</script>
	<!-- start-smoth-scrolling -->
	<script type="text/javascript" src="js/move-top.js"></script>
	<script type="text/javascript" src="js/easing.js"></script>
	<script type="text/javascript">
		jQuery(document).ready(function ($) {
			$(".scroll").click(function (event) {
				event.preventDefault();
				$('html,body').animate({
					scrollTop: $(this.hash).offset().top
				}, 900);
			});
		});
	</script>
	<!-- start-smoth-scrolling -->


	<!-- stats -->
	<script src="js/jquery.waypoints.min.js"></script>
	<script src="js/jquery.countup.js"></script>
	<script>
		$('.counter').countUp();
	</script>
	<!-- //stats -->
	<script type="text/javascript">
		$(document).ready(function () {
			/*
									var defaults = {
							  			containerID: 'toTop', // fading element id
										containerHoverID: 'toTopHover', // fading element hover id
										scrollSpeed: 1200,
										easingType: 'linear' 
							 		};
									*/

			$().UItoTop({
				easingType: 'easeOutQuart'
			});

		});
	</script>
	<a href="#home" class="scroll" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>

	<!-- owl-carousel -->
	<script type="text/javascript" src="js/jquery-2.2.4.min.js"></script>
	<script type="text/javascript" src="js/owl-carousel.js"></script>
	<!-- //End -->


</body>
</html>