<?php
include 'dbconfig.php';
$email = $_SESSION['email'];


$get_about_about_us =$DB_con->prepare(" select * from management WHERE man_id = '1'");
$get_about_about_us->execute();
$about_about_us = $get_about_about_us->fetch();

$get_login =$DB_con->prepare(" select * from login WHERE email = '$email' ");
$get_login->execute();
$login = $get_login->fetch();
?>
<!DOCTYPE html>
<html>

<head>
	<title>Venika | Management </title>
	<!--/tags -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="Conceit Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
	<script type="application/x-javascript">
		addEventListener("load", function () {
			setTimeout(hideURLbar, 0);
		}, false);

		function hideURLbar() {
			window.scrollTo(0, 1);
		}
	</script>
	<!--//tags -->
	<link rel="shortcut icon" href="images/home/venika-icon.png"/>
	<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
	<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
	<link href="css/custom.css" rel="stylesheet" type="text/css" media="all" />
	<link href="css/team.css" rel="stylesheet" type="text/css" media="all" />
	<link href="css/font-awesome.css" rel="stylesheet">
	<!-- //for bootstrap working -->
	<link href="//fonts.googleapis.com/css?family=Raleway:100,100i,200,300,300i,400,400i,500,500i,600,600i,700,800" rel="stylesheet">
	<link href="//fonts.googleapis.com/css?family=Source+Sans+Pro:300,300i,400,400i,600,600i,700" rel="stylesheet">
</head>

<body>
	<!-- header-top -->
	<div class="top_header" id="home">
		<!-- Fixed navbar -->
		<nav class="navbar navbar-default navbar-fixed-top" style="">
			<div class="nav_top_fx_w3ls_agileinfo">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false"
					    aria-controls="navbar">
				        <span class="sr-only">Toggle navigation</span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				    </button>
					<div class="logo-w3layouts-agileits">
						<h1>
							<a class="navbar-brand" href="index.php">
								<!-- <i class="fa fa-clone" aria-hidden="true"></i> Conceit <span class="desc">For your Business</span> -->
								<img src="images/home/venika-logo-head.png" alt="" class="img-responsive">
							</a>
						</h1>
					</div>
				</div>
				<div id="navbar" class="navbar-collapse collapse">
					<div class="nav_right_top">
						<ul class="nav navbar-nav">
							<li><a href="index.php">Home</a></li>
							<li><a href="about.php">About Us</a></li>
							<li class="active"><a href="management.php">Management</a></li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Projects <span class="caret"></span></a>
								<ul class="dropdown-menu">
									<li><a href="under-implementation.php">UNDER IMPLEMENTATION</a></li>
									<li><a href="under-construction.php">UNDER CONSTRUCTION</a></li>
									<li><a href="commissioned.php">COMMISSIONED</a></li>
								</ul>
							</li>
							<li><a href="social-responsibility.php">Social Responsibility</a></li>
							<li><a href="contact.php">Contact</a></li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user"></i>    <?php echo $login['name']; ?> <span class="caret"></span></a>
								<ul class="dropdown-menu">
									<li><a href="logout.php">Logout</a></li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
				<!--/.nav-collapse -->
			</div>
		</nav>
	</div>
	<!-- //End -->

	<!--/banner_info-->
	<div class="banner_inner_con"> </div>
	<div class="services-breadcrumb">
		<div class="inner_breadcrumb">
			<ul class="short">
				<li><a href="index.php">Home</a><span>|</span></li>
				<li>Management</li>
			</ul>
		</div>
	</div>
	<!--//banner_info-->

	<!--/bottom-->
	<div class="banner_bottom">
		<div class="container">
			<div class="title-underline">
				<h3 class="tittle-w3ls" style="margin: 0;"><?php echo $about_about_us['man_title_one']; ?> </h3>
			</div>
			<div class="inner_sec_info_wthree_agile">
				<div class="help_full">
					<div class="col-md-12 banner_bottom_left">
						<h4><?php echo $about_about_us['man_title_two']; ?></h4>
						<p><?php echo $about_about_us['man_desc']; ?></p>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>
	<!--//bottom-->

	<!-- /team -->
	<div class="banner_bottom proj management-proj">
		<div class="container">
			<!-- <div class="title-underline">
				<h3 class="tittle-w3ls">Our Team</h3>
			</div> -->
			<div class="inner_sec_info_wthree_agile" style="margin: 0;">
				<div class="col-md-12 team_grid_info custom_team_grid_info">
					<!-- rehna -->
					<?php
		              $stmt_leadership = $DB_con->prepare('SELECT * FROM  leadership where data_delete = 0 ORDER BY leader_id desc');
		              $stmt_leadership->execute();

		              if($stmt_leadership->rowCount() > 0)
		              {
		              while($leadership=$stmt_leadership->fetch(PDO::FETCH_ASSOC))
		              {
		              extract($leadership);
		              ?>
					<div class="row">
						<div class="col-md-3 div1">
							<h3>Name & Designation</h3>
							<div class="underline1"> </div>
							<img src="admin/db/leader_image/<?php echo $leadership['leader_image']; ?>" alt=" " class="img-responsive" style="height: 150px; width: 150px;" />
							<h3><?php echo $leadership['leader_name']; ?> </h3>
							<p>- <?php echo $leadership['leader_des']; ?></p>
							<div class="team_icons custom-icons">
								<ul>
									<li><a href="#" class="facebook"><i class="fa fa-facebook" aria-hidden="true" ></i></a></li>
									<li><a href="#" class="twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
								</ul>
							</div>
						</div>
						<div class="col-md-6 div2">
							<h3>Relevant experience</h3>
							<div class="underline2"> </div>
							<div class="custom-list">
							<?php echo $leadership['leader_desc']; ?>
							</div>
						</div>
						<div class="col-md-3 div3">
							<h3>Role</h3>
							<div class="underline3"> </div>
							<div class="custom-list">
							<?php echo $leadership['leader_role']; ?>
							</div>
						</div>
						
					</div>
					<?php
			              }
			              }
			              else
			              {
			              ?>
			              	<div class="col-xs-12">
			              		<div class="alert alert-warning">
			              			<span class="glyphicon glyphicon-info-sign"></span> &nbsp; No Data Found ...
			              		</div>
			              	</div>
			              <?php
			              }

			              ?>
					<!-- //End -->
					 
					<!-- <div class="row">
						<div class="col-md-3 div1">
							<h3>Name & Designation</h3>
							<div class="underline1"> </div>
							<img src="images/home/t1.jpg" alt=" " class="img-responsive" />
							<h3>Mr. Chandrasekar D Reddy </h3>
							<p>- Chairman and Managing Director</p>
							<div class="team_icons custom-icons">
								<ul>
									<li><a href="#" class="facebook"><i class="fa fa-facebook" aria-hidden="true" ></i></a></li>
									<li><a href="#" class="twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
								</ul>
							</div>
						</div>
						<div class="col-md-6 div2">
							<h3>Relevant experience</h3>
							<div class="underline2"> </div>
							<div class="custom-list">
								<ul>
									<li>Mr. Chandrasekar D Reddy has over 20 years of experience in the areas of Engineering, Contract and Project Management services</li>
									<li>He started his consultancy services for small hydro projects in the year 2003</li>
									<li>He has also been engaged in business planning for other hydropower project developers for projects with an aggregate capacity over 300 MW</li>
								</ul>
							</div>
						</div>
						<div class="col-md-3 div3">
							<h3>Role</h3>
							<div class="underline3"> </div>
							<div class="custom-list">
								<ul>
									<li><span>Role :</span> <i>Project design, Strategy formulation, Project implementation, Operations management</i></li>
								</ul>
							</div>
						</div>
					</div> -->
					<!-- <div class="row">
						<div class="col-md-3 div1">
							<h3>Name & Designation</h3>
							<div class="underline1"> </div>
							<img src="images/home/t2.jpg" alt=" " class="img-responsive" />
							<h3>Mr. Jagadeeswara D Reddy </h3>
							<p>- Executive Director</p>
							<div class="team_icons custom-icons">
								<ul>
									<li><a href="#" class="facebook"><i class="fa fa-facebook" aria-hidden="true" ></i></a></li>
									<li><a href="#" class="twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
								</ul>
							</div>
						</div>
						<div class="col-md-6 div2">
							<h3>Relevant experience</h3>
							<div class="underline2"> </div>
							<div class="custom-list">
								<ul>
									<li>Mr. Jagadeeswara D Reddy has over 10 years of experience in corporate finance and management</li>
									<li>He has supported multiple organizations for projects in the domain of financial resource mobilization, optimum utilization of funds, budgetary control and investment decisions</li>
									<li>He has supported several projects in the hydropower sector with strategic formulation and financial control</li>
								</ul>
							</div>
						</div>
						<div class="col-md-3 div3">
							<h3>Role</h3>
							<div class="underline3"> </div>
							<div class="custom-list">
								<ul>
									<li><span>Role :</span> <i>Strategy formulation, Financial control and management</i></li>
								</ul>
							</div>
						</div>
					</div> -->
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	<!-- //team -->

	<!--/bottom-->
	<div class="banner_bottom">
		<div class="container">
			<div class="inner_sec_info_wthree_agile" style="margin: 0;">
				<div class="help_full">
					<div class="col-md-12 banner_bottom_left">
						<?php
			              $stmt_adv_board = $DB_con->prepare('SELECT * FROM  adv_board where data_delete = 0 ORDER BY adv_board_id ASC limit 4');
			              $stmt_adv_board->execute();

			              if($stmt_adv_board->rowCount() > 0)
			              {
			              while($row_adv_board=$stmt_adv_board->fetch(PDO::FETCH_ASSOC))
			              {
			              extract($row_adv_board);
			              ?>
		                  <div class="row">
		                     <div class="col-lg-12 about-text-right">
		                        <p><?php echo $row_adv_board['adv_board_desc']; ?></p>
		                     </div>
		                  </div>
			              <?php
			              }
			              }
			              else
			              {
			              ?>
			              <div class="col-xs-12">
			              <div class="alert alert-warning">
			              <span class="glyphicon glyphicon-info-sign"></span> &nbsp; No Data Found ...
			              </div>
			              </div>
			              <?php
			              }

			              ?>
						<!-- <h4>Advisory board profile </h4> -->
						<!-- <p><b>Advisory board profile -</b> Advisory board members of Venika HPPL have been chosen after extensive diligence to ensure congregation of a holistic skill set which includes sectoral expertise, project management, financial management and resource mobilization. Following are the board members of the advisory board for Venika HPPL's small hydropower projects.</p> -->
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>
	<!--//bottom-->
	
	<!-- Footer-start-here -->
	<?php include 'footer.php'; ?>
	<!-- //Footer-end-here -->
	
	<!-- js -->
	<script type="text/javascript" src="js/jquery-2.2.3.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.js"></script>
	<script>
		$('ul.dropdown-menu li').hover(function () {
			$(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
		}, function () {
			$(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
		});
	</script>
	<!-- start-smoth-scrolling -->
	<script type="text/javascript" src="js/move-top.js"></script>
	<script type="text/javascript" src="js/easing.js"></script>
	<script type="text/javascript">
		jQuery(document).ready(function ($) {
			$(".scroll").click(function (event) {
				event.preventDefault();
				$('html,body').animate({
					scrollTop: $(this.hash).offset().top
				}, 900);
			});
		});
	</script>
	<!-- start-smoth-scrolling -->


	<!-- stats -->
	<script src="js/jquery.waypoints.min.js"></script>
	<script src="js/jquery.countup.js"></script>
	<script>
		$('.counter').countUp();
	</script>
	<!-- //stats -->
	<script type="text/javascript">
		$(document).ready(function () {
			/*
									var defaults = {
							  			containerID: 'toTop', // fading element id
										containerHoverID: 'toTopHover', // fading element hover id
										scrollSpeed: 1200,
										easingType: 'linear' 
							 		};
									*/

			$().UItoTop({
				easingType: 'easeOutQuart'
			});

		});
	</script>
	<a href="#home" class="scroll" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>

</body>

</html>