<?php
//include config
require_once('config/config.php');
//check if already logged in
if( $user->is_logged_in() ){ header('Location: home.php'); } 
?>
<!DOCTYPE HTML>
<html lang="zxx">
<head>
	<title>Venika :: Portal</title>
	<link rel="shortcut icon" href="images/logo_icon.png"/>
	<!-- Meta tag Keywords -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="Alicon Portal Login."
	/>
	<script>
		addEventListener("load", function () {
			setTimeout(hideURLbar, 0);
		}, false);

		function hideURLbar() {
			window.scrollTo(0, 1);
		}
	</script>
	
	<!-- Meta tag Keywords -->
	
	<!-- css files -->
	<link rel="stylesheet" href="admin/css/css_login/style.css" type="text/css" media="all" />
	<link rel="stylesheet" href="admin/css/css_register/style.css" type="text/css" media="all" />
	<!-- Style-CSS -->
	<link rel="stylesheet" href="admin/css/css_login/font-awesome.css">
	<!-- Font-Awesome-Icons-CSS -->
	<!-- //css files -->
	<!-- Sign-up -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
	<!-- //End -->
	<!-- online-fonts -->
	<link href="//fonts.googleapis.com/css?family=Tangerine:400,700" rel="stylesheet">
	<link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
	<!-- //online-fonts -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

	
</head>

<body>
	<!--//header-->
	<div class="main-content-agile index-bg">
		<div class="sub-main-w3" style="margin-top: 200px;">
			<h3 style="font-size: 30px; color:#fff;">Are you a valid user ?</h3>
			<!-- <img src="admin/images/venika_logo.png" alt="" class="img-responsive" style="width: 200px; display: inline-block;" /> -->
			<!-- <h2>Welcome To Alicon</h2> -->
			<h2><p class="statusMsg"></p></h2>
			<!-- <h2><?php echo $_SESSION['sess_msg'];?></h2> -->
			<?php
				//process login form if submitted
				if(isset($_POST['submit']))
				{
					$email = trim($_POST['email']);
					
					/*$password = trim($_POST['account_password']);*/

					if($user->login($email))
					{ 
					//logged in return to index page
						header('Location: index.php');
						exit;
					} 
					else
					{
						$message = '<p class="error" style="color:red;">Incurrect Emailid</p>';
					}
				}//end if submit
				if(isset($message)){ echo $message; }
			?>
			<form class="form-horizontal" enctype="multipart/form-data" method="post">
				<div class="pom-agile form-group">
					<span class="fa fa-user-o" aria-hidden="true"></span>
					<input placeholder="Email address" name="email" class="user" id="email" type="text" required="">
				</div>
				
				<div class="right-w3l form-group">
					<!--<input type="button" name="submit" onclick="loginFuncton()"  value="Login">-->
					<input type="submit" name="submit" value="Submit">
					
				</div>
				<div>
					<a data-toggle="modal" data-target="#myModal" style="cursor: pointer; color: #fff;">Register New</a>
				</div>
			</form>
		</div>
	</div>
	<!--//main-->

	<!-- Sign-up-popup -->
	<!-- Modal -->
	  <div class="modal fade" id="myModal" role="dialog">
	    <div class="modal-dialog">
	      <div class="modal-content">
	        <div class="modal-header">
	          <button type="button" class="close" data-dismiss="modal">&times;</button>
	          <h4 class="modal-title">Registation Form</h4>
	        </div>
	        <div class="modal-body">
	          <form class="form-controls" enctype="multipart/form-data" id="insert_form" method="post">
	          	<div class="form-group">
			    <label for="pwd">User name:</label>
			    <input type="text" class="form-control" id="name" name="name" placeholder="Email name">
			  </div>
			  <div class="form-group">
			    <label for="email">Email address:</label>
			    <input type="email" class="form-control" id="email" name="email" placeholder="Email address">
			  </div>
			  <div class="modal-footer myFooter">
		          <input type="submit" name="submit" class="btn btn-info submitBtn" value="Submit"/>
		          <a href="index.php" class="btn btn-danger" data-dismiss="modal">Close</a>
		          <p class="statusMsg"></p>
	           </div>
			  </form>
	        </div>

	      </div>
	    </div>
	  </div>
	<!-- //End -->

	<!--footer-->
	
	<!-- //End -->

	<!-- Sign-up -->
	
	<!-- //End -->
	<script>
   $(document).ready(function(e){
    $("#insert_form").on('submit', function(e){
        e.preventDefault();
      
        $.ajax({
            type: 'POST',
            url: 'admin/db/register.php',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData:false,
            beforeSend: function(){
                $('.submitBtn').attr("disabled","disabled");
                $('#insert_form').css("opacity",".5");
            },
            success: function(msg){
                $('.statusMsg').html('');
                if(msg == 'ok'){
                    $('#insert_form')[0].reset();
                    $('.statusMsg').html('<span style="font-size:15px;color:#108eaf">Registred successfully. <img src="admin/images/tick_677914.png" style="width: 30px; height: 30px;"></span>');
                    setTimeout(function () {
   						      window.location.href= 'index.php'; // the redirect goes here

					},1000);

                }

                else{
                    $('.statusMsg').html('<span style="font-size:15px;color:#EA4335">Some problem occurred, please try again.</span>');
                }
                $('#insert_form').css("opacity","");
                $(".submitBtn").removeAttr("disabled");
            }
        });
    });
});

</script>
</body>

</html>
                             