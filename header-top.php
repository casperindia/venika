<!-- header-top -->
<div class="top_header" id="home">
	<!-- Fixed navbar -->
	<nav class="navbar navbar-default navbar-fixed-top" style="border-bottom: 2px solid #118eaf;">
		<div class="nav_top_fx_w3ls_agileinfo">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false"
				    aria-controls="navbar">
			        <span class="sr-only">Toggle navigation</span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			    </button>
				<div class="logo-w3layouts-agileits">
					<h1>
						<a class="navbar-brand" href="index.php">
							<!-- <i class="fa fa-clone" aria-hidden="true"></i> Conceit <span class="desc">For your Business</span> -->
							<img src="images/home/venika-logo.png" alt="" class="">
						</a>
					</h1>
				</div>
			</div>
			<div id="navbar" class="navbar-collapse collapse">
				<div class="nav_right_top">
					<!-- <ul class="nav navbar-nav navbar-right">
						<li><a class="request" href="contact.php">Send Request</a></li>
					</ul> -->
					<!-- <ul class="nav navbar-nav navbar-right social-nav model-3d-0 footer-social"> -->
					<!-- <ul class="social-nav model-3d-0 footer-social social two"> -->
						<!-- <li style="float: left; border: 1px solid rgb(118, 218, 255); padding: 1px;">
							<a href="#" class="facebook" style="padding: 7px 0;">
								<div class="front"><i class="fa fa-facebook" aria-hidden="true" style="margin-top: 10px; color: #fff;"></i></div>
								<div class="back"><i class="fa fa-facebook" aria-hidden="true" style="margin-top: 10px;"></i></div>
							</a>
						</li>
						<li style="float: left; border: 1px solid rgb(118, 218, 255); padding: 1px;">
							<a href="#" class="twitter" style="padding: 7px 0;">
								<div class="front"><i class="fa fa-twitter" aria-hidden="true" style="margin-top: 10px; color: #fff;"></i></div>
								<div class="back"><i class="fa fa-twitter" aria-hidden="true" style="margin-top: 10px;"></i></div>
							</a>
						</li>
						<li style="float: left; border: 1px solid rgb(118, 218, 255); padding: 1px;">
							<a href="#" class="instagram" style="padding: 7px 0;">
								<div class="front"><i class="fa fa-instagram" aria-hidden="true" style="margin-top: 10px; color: #fff;"></i></div>
								<div class="back"><i class="fa fa-instagram" aria-hidden="true" style="margin-top: 10px;"></i></div>
							</a>
						</li>
						<li style="float: left; border: 1px solid rgb(118, 218, 255); padding: 1px;">
							<a href="#" class="pinterest" style="padding: 7px 0;">
								<div class="front"><i class="fa fa-linkedin" aria-hidden="true" style="margin-top: 10px; color: #fff;"></i></div>
								<div class="back"><i class="fa fa-linkedin" aria-hidden="true" style="margin-top: 10px;"></i></div>
							</a>
						</li>
					</ul> -->
					<ul class="nav navbar-nav">
						<li class="active"><a href="index.php">Home</a></li>
						<li><a href="#">About Us</a></li>
						<li><a href="#">Management</a></li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Projects <span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><a href="#">UNDER Implimentation</a></li>
								<li><a href="#">UNDER Construction</a></li>
								<li><a href="#">Commissioned</a></li>
							</ul>
						</li>
						<li><a href="#">Social Responsibility</a></li>
						<li><a href="#">Contact</a></li>
						<li><a href="#"><i class="fa fa-power-off" aria-hidden="true"></i></a></li>
					</ul>
				</div>
			</div>
			<!--/.nav-collapse -->
		</div>
	</nav>
</div>
<!-- //End -->